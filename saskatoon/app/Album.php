<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Album extends Model
{
    protected $fillable = [
        'name',
        'type',
        'place',
        'main_photo',
        'position',
        'status',
        'created_at'
    ];


    public function photos()
    {
        return $this->hasMany('App\Photo');
    }


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($album) {
            if ($album->main_photo) Storage::disk('upload')->delete($album->main_photo);

            foreach ($album->photos as $photo)
                Storage::disk('upload')->delete($photo->image);
            $album->photos()->delete();
        });
    }
}

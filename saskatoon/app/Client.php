<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Client extends Model
{
    protected $fillable = [
        'logo',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($client) {
            if ($client->logo) Storage::disk('upload')->delete($client->logo);
        });
    }
}

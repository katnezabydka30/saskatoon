<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\Constraint;

class AdminController extends Controller
{

    public function savePosition($album_id, $type){
        $album = Album::find($album_id);
        $max_position = Album::select('position')->where('type', $type)->max('position');
        if ($album->update(['position' => (int)$max_position + 1])) return true;
        else return false;
    }


    public function uploadImage($file, $size_path, $folder)
    {
        $path = $folder . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk('upload')->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();


        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png'])) {
            $image = Image::make($file)
                ->resize($size_path['width'], $size_path['height'], function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->encode($file->getClientOriginalExtension(), 100);

            // move uploaded file from temp to uploads directory
            if (Storage::disk('upload')->put($fullPath, (string)$image, 'public')) {
                $status = true;
                $fullFilename = $fullPath;
            } else {
                $status = 'Ошибка загрузки изображения';
            }
        } else {
            $status = 'Неправильный тип изображения';
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return ['status' => $status, 'fullFileName' => $fullFilename];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Album;
use App\Http\Requests\AlbumRequest;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class AlbumController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('admin.gallery.show', 'residential');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {
        $data['name'] = $request->name;
        $data['type'] = $request->type;
        $data['place'] = $request->place;

        $max_position = Album::select('position')->where('type', $request->type)->max('position');
        $data['position'] = (int)$max_position + 1;
        $request->created_at ? $data['created_at'] = $request->created_at : null;

        if ($request->hasFile('main_photo')) {
            $image = $this->uploadImage($request->file('main_photo'), Config::get('settings.photo'), 'gallery/' . $request->name);
            if ($image['status'] === true) {
                $data['main_photo'] = $image['fullFileName'];
            } else
                return redirect()->back()->withErrors(['main_photo' => $image['status']]);
        }

        $album = Album::create($data);

        if ($request->hasFile('photo')) {
            $image = $this->uploadImage($request->file('photo'), Config::get('settings.photo'), 'gallery/' . $request->name);
            if ($image['status'] === true) {
                Photo::create([
                    'album_id' => $album->id,
                    'image' => $image['fullFileName'],
                ]);
            } else
                return redirect()->back()->withErrors(['photo' => $image['status']]);
        }
        $count = explode(',', $request->count);
        if (count($count) > 0) {
            for ($i = 0; $i < count($count); $i++) {
                if ($request->hasFile('photo-' . $i)) {
                    $image = $this->uploadImage($request->file('photo-' . $i), Config::get('settings.photo'), 'gallery/' . $request->name);
                    if ($image['status'] === true) {
                        Photo::create([
                            'album_id' => $album->id,
                            'image' => $image['fullFileName'],
                        ]);
                    } else
                        return redirect()->back()->withErrors(['photo' => $image['status']]);
                }
            }
        }

        if ($album)
            return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => '']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($type)
    {
        //search
        if (Input::has('search')) {
            $search = Input::get('search');

            $albums = Album::with('photos')
                ->where(function ($query) use ($search, $type) {
                    $query->where('name', 'like', '%' . $search . '%')->where('type', $type);
                })
                ->orWhere(function ($query) use ($search, $type) {
                    $query->where('place', 'like', '%' . $search . '%')->where('type', $type);
                })
                ->orderBy('position')
                ->paginate(Config::get('settings.count_page'));
            $albums_all = Album::orderBy('position')->where('type', $type)->get();
            return view('admin.gallery', compact('albums', 'albums_all', 'type'));

        } else {
            $albums = Album::with('photos')->where('type', $type)->orderBy('position')->paginate(Config::get('settings.count_page'));
            $albums_all = Album::orderBy('position')->where('type', $type)->get();
            return view('admin.gallery', compact('albums', 'albums_all', 'type'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        $album = Album::find($id);
        $data = $request->all();

        if ($request->hasFile('main_photo')) {
            $image = $this->uploadImage($request->file('main_photo'), Config::get('settings.photo'), 'gallery/' . $request->name);

            if ($image['status'] === true) {
                $data['main_photo'] = $image['fullFileName'];
                Storage::disk('upload')->delete($album->main_photo);
            } else
                return redirect()->back()->withErrors(['main_photo' => $image['status']]);
        }
        $album->update($data);
//
//        // Удаляем изображения которые пришли из массива удаленных

        $deleted_images = explode(',', $data['deleted_images']);
//        $data2['delete'] = $deleted_images;
//        $data2['album_photos'] = $album->photos;
//        $data2['count_deleted_images'] = count($deleted_images);
        if (count($deleted_images) > 0) {
                foreach ($album->photos as $photo){
//                    $data2['photo-' . $photo->id] = $photo;
//                    $data2['$deleted_images'] = $deleted_images;
                    if (in_array($photo->id, $deleted_images)){
//                        $data2[$photo->id] = in_array($photo->id, $deleted_images);
//                        $data2[$photo->id] = array_search($photo->id, $deleted_images);
                        // Удалили фото
                        Storage::disk('upload')->delete($photo->image);
                        // Удалили связь
                        $photo->delete();
                    }
                }

        }

        // Добавляем фото в альбом если пришли новые

        if ($request->hasFile('photo')) {
            $image = $this->uploadImage($request->file('photo'), Config::get('settings.photo'), 'gallery/' . $request->name);
            if ($image['status'] === true) {
                Photo::create([
                    'album_id' => $album->id,
                    'image' => $image['fullFileName'],
                ]);
            } else
                return redirect()->back()->withErrors(['photo' => $image['status']]);
        }


        $count = explode(',', $request->count);
        if (count($count) > 0) {
            for ($i = 0; $i < count($count); $i++) {
                if ($request->hasFile('photo-' . $i)) {
                    $image = $this->uploadImage($request->file('photo-' . $i), Config::get('settings.photo'), 'gallery/' . $request->name);
                    if ($image['status'] === true) {
                        Photo::create([
                            'album_id' => $album->id,
                            'image' => $image['fullFileName'],
                        ]);
                    } else
                        return redirect()->back()->withErrors(['photo' => $image['status']]);
                }
            }
        }


        if ($album)
        return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => '']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $album = Album::find($id);
        if ($album->delete())
            return redirect()->back();
        else  return redirect()->back()->withErrors('Something wrong');
    }

    public function block($id)
    {
        $album = Album::find($id);
        ($album->status) ? $album->update(['status' => 0]) : $album->update(['status' => 1]);
        return redirect()->back();

    }

    public function changePosition($id, Request $request)
    {

        $album = Album::find($id);

        if ($album->position != $request->position) {

            $album_old = Album::where('position', $request->position)->first();

            if ($album_old) $album_old->update(['position' => $album->position]);

            $album->update(['position' => $request->position]);

            if ($album) return redirect()->route('admin.gallery.index');
            else  return redirect()->back()->withErrors(['save' => 'something wrong']);

        }

        return redirect()->back();
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class ClientController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return view('admin.client', compact('clients'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        $photo = '';
        if ($request->hasFile('logo')) {

            $image = $this->uploadImage($request->file('logo'), Config::get('settings.photo'), 'clients/');
            if ($image['status'] === true) {
                $photo = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['photo' => $image['status']]);
            }

        }

        $client = Client::create(['logo' => $photo]);

        if ($client)
            return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => '']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, $id)
    {

        $client = Client::find($id);
        if ($request->hasFile('logo')) {

            $image = $this->uploadImage($request->file('logo'), Config::get('settings.photo'), 'clients/');
            if ($image['status'] === true) {
                $photo = $image['fullFileName'];
                Storage::disk('upload')->delete($client->logo);
            } else {
                return redirect()->back()->withErrors(['photo' => $image['status']]);
            }

        }

        $client->update(['logo' => $photo]);

        if ($client)
            return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => '']);
    }

    /**
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Client $client)
    {
        if ($client->delete())
            return redirect()->back();
        else  return redirect()->back()->withErrors('Something wrong');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function block($id)
    {
        $client = Client::find($id);
        ($client->status) ? $client->update(['status' => 0]) : $client->update(['status' => 1]);
        return redirect()->back();
    }
}

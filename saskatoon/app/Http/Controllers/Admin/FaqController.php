<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\Http\Requests\FaqRequest;
use Illuminate\Support\Facades\Config;

class FaqController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(Config::get('settings.count_page'));
        return view('admin.faq', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request)
    {
        $data['question'] = $request->question;
        $data['answer'] = $request->answer;
        $request->created_at ? $data['created_at'] = $request->created_at : null;
        $faq = Faq::create($data);
        if ($faq) return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => '']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Faq $faq
     * @return \Illuminate\Http\Response
     */
    public function update(FaqRequest $request, Faq $faq)
    {
        $data = $request->except('_token', '_method');
        if ($faq->update($data))
            return response(['status' => 'success', 'message' => '', 'data' => $data]);
        else  return response(['status' => 'error', 'message' => 'Something wrong']);
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Faq $faq)
    {
        if ($faq->delete())
            return redirect()->back();
        else  return redirect()->back()->withErrors('Something wrong');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function block($id)
    {
        $faq = Faq::find($id);
        ($faq->status) ? $faq->update(['status' => 0]) : $faq->update(['status' => 1]);
        return redirect()->back();
    }
}

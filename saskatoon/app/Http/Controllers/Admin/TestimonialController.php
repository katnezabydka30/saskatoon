<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TestimonialRequest;
use App\Testimonial;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class TestimonialController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::orderBy('created_at', 'desc')->paginate(Config::get('settings.count_page'));
        return view('admin.testimonials', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestimonialRequest $request)
    {
        $data['name'] = $request->name;
        $data['place'] = $request->place;
        $data['comment'] = $request->comment;
        $request->created_at ? $data['created_at'] = $request->created_at : null;


        if ($request->hasFile('image')) {
            $image = $this->uploadImage($request->file('image'), Config::get('settings.photo'), 'testimonial');
            if ($image['status'] === true) {
                $data['image'] = $image['fullFileName'];
            } else {
                return redirect()->back()->withErrors(['image' => $image['status']]);
            }
        }

        $testimonial = Testimonial::create($data);
        if ($testimonial)
            return response(['status' => 'success', 'message' => '', 'data' => $request->all()]);
        else  return response(['status' => 'error', 'message' => '']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(TestimonialRequest $request, Testimonial $testimonial)
    {
        $data = $request->except('_token', '_method');

        if ($request->del_img) {
            Storage::disk('upload')->delete($testimonial->image);
        }

        if ($request->hasFile('image')) {
            $image = $this->uploadImage($request->file('image'), Config::get('settings.photo'), 'testimonial');
            if ($image['status'] === true) {
                $data['image'] = $image['fullFileName'];
                Storage::disk('upload')->delete($testimonial->image);
            } else {
                return redirect()->back()->withErrors(['image' => $image['status']]);
            }
        }

        if ($testimonial->update($data))
            return response(['status' => 'success', 'message' => '']);
        else  return response(['status' => 'error', 'message' => 'Something wrong']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        if ($testimonial->delete())
            return redirect()->back();
        else  return redirect()->back()->withErrors('Something wrong');
    }

    public function block($id)
    {
        $testimonial = Testimonial::find($id);
        ($testimonial->status) ? $testimonial->update(['status' => 0]) : $testimonial->update(['status' => 1]);
        return redirect()->back();
    }
}

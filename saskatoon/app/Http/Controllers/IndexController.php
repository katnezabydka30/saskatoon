<?php

namespace App\Http\Controllers;


use App\Album;
use App\Client;
use App\Contact;
use App\Event;
use App\Faq;
use App\Http\Requests\MailRequest;
use App\Partner;
use App\Speaker;
use App\SpeakerMain;
use App\Testimonial;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Jenssegers\Date\Date;


class IndexController extends Controller
{

    public function index()
    {
        $clients = Client::where('status', true)->orderBy('created_at', 'desc')->take(10)->get();
        $testimonials = Testimonial::where('status', true)->orderBy('created_at', 'desc')->take(10)->get();

        return view('index', compact('testimonials', 'clients'));
    }

    public function faq()
    {

        $faqs = Faq::where('status', true)->orderBy('created_at', 'desc')->get();
        return view('faq', compact('faqs'));
    }

//    public function galleryCommercial()
//    {
//        $albums = Album::with('photos')->where('type', 'commercial')->orderBy('position')->get();
//        return view('gallery-commercial', compact('albums'));
//    }
//
//    public function galleryResidential()
//    {
//        $albums = Album::with('photos')->where('type', 'residential')->orderBy('position')->get();
//        return view('gallery-residential',  compact('albums'));
//    }

    public function gallery($type)
    {
        if ($type == 'residential') {
            $albums = Album::with('photos')->where('type', 'residential')->where('status', true)->orderBy('position')->get();
            return view('gallery', compact('albums'));
        }
        if ($type == 'commercial') {
            $albums = Album::with('photos')->where('type', 'commercial')->where('status', true)->orderBy('position')->get();
            return view('gallery', compact('albums'));
        }
//
//        $albums = Album::with('photos')->where('type', 'residential')->orderBy('position')->get();
//        return view('gallery',  compact('albums'));
    }

    public function testimonial()
    {
        $testimonials = Testimonial::where('status', true)->orderBy('created_at', 'desc')->get();

        return view('testimonials', compact('testimonials'));

    }

    public function nowHiring()
    {

        return view('now-hiring');
    }

    public function booking()
    {

        return view('booking');
    }


//    public function contactForm(MailRequest $request)
//    {
//        $contact = Contact::first();
//        $email = $contact->email ? $contact->email : env('MAIL_USERNAME_TO');
//        Mail::send('mailContact', ["request" => $request], function ($message) use ($email) {
//            $message->from(env('MAIL_USERNAME'), 'Contact form');
//            $message->to($email, 'Contact form')->subject('Contact form');
//        });
//
//        if (count(Mail::failures()) > 0)
//            return response(['status' => 'failure', 'message' => 'Something wrong', 'data' => Mail::failures()]);
//        else
//            return response(['status' => 'success', 'message' => '']);
//    }

}

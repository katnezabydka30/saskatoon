<?php

namespace App\Http\Controllers;


class ServiceController extends Controller
{
    public function services()
    {
        return view('services/services');
    }

    public function serviceHouse()
    {
        return view('services/service-house-cleaning');
    }

    public function serviceOffice()
    {
        return view('services/service-office-cleaning');
    }

    public function serviceMoving()
    {
        return view('services/service-moving-day-cleaning');
    }

    public function servicePreparation()
    {
        return view('services/service-preparation-for-sale');
    }

    public function serviceCarpet()
    {
        return view('services/service-washing-carpet');
    }

    public function serviceWindows()
    {
        return view('services/service-washing-windows');
    }

    public function serviceGrand()
    {
        return view('services/service-grand');
    }

    public function serviceDeep()
    {
        return view('services/service-deep-cleaning');
    }

    public function servicePostConstruction()
    {
        return view('services/service-post-construction-clean-up');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'album_id',
        'image',
    ];

    public function album() {
        return $this->belongsTo('App\Album');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Testimonial extends Model
{
    protected $fillable = [
        'name',
        'image',
        'place',
        'comment',
        'status',
    ];

    public static function boot() {
        parent::boot();

        static::deleting(function($testimonial) {
            if ($testimonial->image) Storage::disk('upload')->delete($testimonial->image);
        });
    }

}

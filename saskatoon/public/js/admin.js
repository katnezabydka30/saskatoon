(() => {
    const HEADERS = {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    };
    const pageId = $('main').attr('id');

    switch (pageId) {
        case 'testimonialsAdmin': {
            testimonialsEdit();
            inputImage();
            // deleteInputImg();
            break;
        }
        case 'faqsAdmin': {
            testimonialsEdit();
            break;
        }
        case 'galleryAdmin': {
            inputImage();
            tabulImages();
            galleryAdminModal();
            tabulTable();
            dublicateImage();

            break;
        }
        case 'clientsAdmin': {
            inputImage();
            openerClient();
            // deleteInputImg();
            break;
        }
    }

    // Admin edit modal
    function testimonialsEdit() {
        $(document).ready(function () {
            $('.settingAdmin_block .edit_modal').css('display', 'block');
        });
        $('.edit_btn').click(function () {
            $('.edit_modal').addClass('active');
            $('section.settingsAdmin_section .settingAdmin_block').addClass('noScroll');
            var that = $(this);

            var obj = {
                name: that.parents('.testimonials_table_body').find('.name_test').text().trim(),
                place: that.parents('.testimonials_table_body').find('.place_test').text().trim(),
                comment: that.parents('.testimonials_table_body').find('.comment_test').text().trim(),
                question: that.parents('.testimonials_table_body').find('.faq_quastion').text().trim(),
                answer: that.parents('.testimonials_table_body').find('.faq_answer').text().trim(),
                date: that.parents('.testimonials_table_body').find('.name_date').text().trim()
            };
            let dataVal = $(this).parents(".testimonials_table_body").find(".name_date").attr('data-value').trim();
            console.log(dataVal);
            
            // console.log(dataVal);
            $('.edit_modal').find('.modal_question').val(obj.question);
            $('.edit_modal').find('.modal_answer').val(obj.answer);
            // $('.edit_modal_container_item .input_box').find('#date').attr('value', obj.date.replace(/\./g, '-'));
            $('#date').attr('value', dataVal);
            $('.edit_modal').find('.name_area').val(obj.name);
            $('.edit_modal').find('.place_area').val(obj.place);
            $('.edit_modal').find('.comment_area').val(obj.comment);
        });
        $('.create_testimonial').on('click', function () {
            var dateNow = new Date();
            var Y = dateNow.getFullYear();
            var M = Number(dateNow.getMonth()) + 1;
            var D = dateNow.getDate();
            var fullDate = `${Y}-0${M}-${D}`;

            $('.edit_modal').addClass('active');
            $('section.settingsAdmin_section .settingAdmin_block').addClass('noScroll');
            $('.edit_modal').find('.modal_question').val('');
            $('.edit_modal').find('.modal_answer').val('');
            // $('.edit_modal_container_item .input_box').find('#date').attr('value', obj.date.replace(/\./g, '-'));
            $('#date').attr('value', fullDate);
            $('.edit_modal').find('.name_area').val('');
            $('.edit_modal').find('.place_area').val('');
            $('.edit_modal').find('.comment_area').val('');
            $('.edit_modal').find('.edit_modal_container_label_image_item').attr('src', '');
            $('.edit_modal').find('.input_file_close').removeClass('active');
        });
        $('.close_edit_modal').click(function () {
            $('.edit_modal').removeClass('active');
            $('section.settingsAdmin_section .settingAdmin_block').removeClass('noScroll');
        });
        $('.edit_modal .overlay').click(function () {
            $('.edit_modal').removeClass('active');
            $('section.settingsAdmin_section .settingAdmin_block').removeClass('noScroll');
        });
    }

    // Gallery Input Image
    function inputImage() {

        function readURL(input, image) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // console.log(e.target.result);


                    $(image).attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".tabul_container_item_input").change(function () {
            var image = $(this).parent().find('.tabul_container_item_image');
            readURL(this, image);
        });

        $("#imageFile1").change(function () {
            var image = $(this).parents('.input_box_image_file').find('.edit_modal_container_label_image_item');
            $(this).parents('.input_box_image_file').find('.input_file_close').addClass('active');
            readURL(this, image);
        });


    }

    function openerClient(){
        $('.addClient').click(function(){
            $('.clientAdmin_modal').addClass('active');
        });
        $('.edit_client').click(function(){
            $('.clientAdmin_modal').addClass('active');
            $('.clientAdmin_modal_container input[name="_method"]').val('PUT');
        });
        $('.clientAdmin_modal .clients_closer').click(function(){
            $('.clientAdmin_modal').removeClass('active');
        });
        $('.clientAdmin_modal_overlay').click(function(){
            $('.clientAdmin_modal').removeClass('active');
        });
    }

    function tabulImages() {
        $('[data-tabul]').click(function () {
            var target = $(this).attr('data-tabul');

            $('.galleryAdmin_modal_container .tabul').removeClass('active');
            $('.galleryAdmin_modal_container .tabul' + target).addClass('active');


        });
    }

    function tabulTable() {
        $('[data-table]').click(function () {
            var target = $(this).attr('data-table');

            $('.gallery_tabs_item').removeClass('active');
            $(this).addClass('active');
            $('.testimonials_table').removeClass('active');
            $('.testimonials_table' + target).addClass('active');
        });
    }

    // Gallery Admin Modal
    function galleryAdminModal() {
        $(document).ready(function () {
            $('.galleryAdmin_modal').css('display', 'block');
        });
        $('section.galleryAdmin_section .edit_btn').click(function () {
            $('.galleryAdmin_modal').addClass('active');
            var that = $(this);
            var obj = {
                name: that.parents('.testimonials_table_body').find('.album_name').text().trim(),
                place: that.parents('.testimonials_table_body').find('.name_place').text().trim(),
                date: that.parents('.testimonials_table_body').find('.name_date').text().trim()
            };

            $('.galleryAdmin_modal').find('.album_name').val(obj.name);
            $('.galleryAdmin_modal').find('.place_input').val(obj.place);
            // $('.galleryAdmin_modal').find('.date_input').val(obj.date);

        });

        $('.gallery_tabs_addItem_btn').on('click', function () {

            $('.galleryAdmin_modal').addClass('active');
            var dateNow = new Date();
            var Y = dateNow.getFullYear();
            var M = Number(dateNow.getMonth()) + 1;
            var D = dateNow.getDate();
            var fullDate = `${Y}-0${M}-${D}`;

            // $('.edit_modal_container_item .input_box').find('#date').attr('value', obj.date.replace(/\./g, '-'));
            
            $('.galleryAdmin_modal').find('.album_name').val('');
            $('.galleryAdmin_modal').find('.place_input').val('');
            // $('.galleryAdmin_modal').find('.date_input').val('');
            $('#date').attr('value', fullDate);
            $('.tabul_container_item.main_image').find('.tabul_container_item_image').attr('src', '');
        });

        $('.galleryAdmin_modal_container_closer').click(function () {
            $('.galleryAdmin_modal').removeClass('active');
        });
    }

    let deleteArr = [];
    let imgVal;

    function deleteImage() {

        imgVal = $(this).parent().find('.hidden_del_img').val();
        deleteArr.push(imgVal);
        // console.log(deleteArr);
        $('[name=deleted_images]').val(deleteArr);

        $(this).parent().remove();

        var arrEditImg = $('.tabul_container_item');
        console.log(arrEditImg);

        if (arrEditImg.length <= 1) {
            $('.add_box').before(`<div class="col-12 col-sm-6 col-lg-4 image_box static_image">
                                    <label class="tabul_container_item">
                                        <input class="tabul_container_item_input" type="file" name="photo">
                                        <img src="#" alt="" class="tabul_container_item_image">
                                    </label>
                                    <!--<input class="hidden_del_img" type="hidden" name="delete_image" value="">-->
                                </div>`);
                                inputImage();

        }
    }

    // function returnImage(){
    //     var arrEditImg = $('.tabul_container_item');
    //     if

    // }
    // ------------------

    let deleteArrReview = [];
    let imgValReview;

    $('.input_file_close').click(function () {

        // imgValReview = $(this).parent().find('.hidden_del_review_img').val();
        // deleteArrReview.push(imgValReview);
        // // console.log(deleteArrReview);

        // $('[name=_method]').val(deleteArrReview);

        // console.log($('[name=_method]').val());


        $(this).parent().find('.edit_modal_container_label_image_item').attr('src', '');
        $(this).removeClass('active');
        // $(this).parent().remove();
    });

    // Dublicate Gallery Images
    function dublicateImage() {
        let arrImgFile = [];
        let imageCounter = 0;
        $('.add_box').click(function () {
            let newImg = $(`<div class="col-12 col-sm-6 col-lg-4 image_box added">
                                <label class="tabul_container_item">
                                    <input class="tabul_container_item_input" type="file" name="photo-${imageCounter++}">
                                    <img src="#" alt="" class="tabul_container_item_image">
                                </label>
                                <img src="../../../public/images/close.png" alt="" class="tabul_container_item_close">
                                <!--<input class="hidden_del_img" type="hidden" name="delete_image" value="${Math.floor(Math.random() * 1000)}">-->
                            </div>`);
            newImg.find('.tabul_container_item_close').click(deleteImage);
            $(this).before(newImg);
            arrImgFile.push(imageCounter - 1);
            $('.count').val(arrImgFile);
            inputImage();
        });
    }

    $('.create_faq button, .create_testimonial button, .create_gallery button').on('click', function () {
        let url = $(this).attr('data-action');
        $('.edit_modal_container').attr('action', url);
        $('.edit_modal_container .method').val('');

    });

    $('.edit-faq button, .edit-testimonial button, .edit_gallery button').on('click', function () {
        let url = $(this).attr('data-action');
        $('.edit_modal_container').attr('action', url);
        $('.edit_modal_container .method').val('put');
    });
    $('.edit-testimonial').click(function () {
        var reviewImage = $(this).parents('.testimonials_table_body').find('.table_favic_image').attr('src');


        $('.edit_modal_container_item .edit_modal_container_label_image_item').attr('src', reviewImage);
        var srcInpImage = $('.edit_modal_container_label_image_item').attr('src');

        if (srcInpImage == '') {
            $('.input_box_image_file .input_file_close').removeClass('active');
        } else {
            $('.input_box_image_file .input_file_close').addClass('active');
        }


    });
    $('.edit_client').click(function(){
        let client_id = $(this).find('button').attr('data-action');

        $('.clientAdmin_modal_container').attr('action', client_id);
        console.log(client_id);

        let imgSrc = $(this).parents('.testimonials_table_body').find('.main_picture_img').attr('src');
        $('.clientAdmin_modal_container').find('.tabul_container_item_image').attr('src', imgSrc);
    });
    $('.edit-gallery').click(function () {
        let album_id = $(this).find('button').attr('data-id');
        $('input[name=album_id]').val(album_id);
        $('input.method').val('PUT');

        let action_edit = $(this).find('button').attr('data-action');
        console.log(action_edit);
        $('.galleryAdmin_modal_container_form').attr('action', action_edit);

        let dataVal = $(this).parents(".testimonials_table_body").find(".name_date").attr('data-value').trim();
        console.log(dataVal);
        
        $('#date').attr('value', dataVal);

        var main_image = $(this).parents('.testimonials_table_body').find('.main_picture_img').attr('src');
        var sub_images = $(this).parents('.testimonials_table_body').find('.hidden-galery p');

        var imgArr = new Map();


        $(sub_images).each(function (key, item) {
            var srcImage = $(item).attr('src');
            var idImage = $(item).attr('data-id');
            imgArr.set(idImage, srcImage);
        });

        var imageCounter = 0;
        // console.log(imgArr.entries());
        
        for (let pair of imgArr.entries()) {
            let id = pair[0];
            let src = pair[1];
            let newImg = $(`<div class="col-12 col-sm-6 col-lg-4 image_box added">
                                    <label class="tabul_container_item">
                                        <input class="tabul_container_item_input" type="file" name="photo-${imageCounter++}" value="${src}">
                                        <img src="${src}" alt="" class="tabul_container_item_image">
                                    </label>
                                    <img src="../../../public/images/close.png" alt="" class="tabul_container_item_close">
                                    <input class="hidden_del_img" type="hidden" name="delete_image" value="${id}">
                                </div>`);
            $('.add_box').before(newImg);
            $('.static_image').remove();
            newImg.find('.tabul_container_item_close').click(deleteImage);
            // newImg.find('.tabul_container_item_close').click(returnImage);
            inputImage();
        }

        $('.tabul_container_item.main_image').find('.tabul_container_item_image').attr('src', main_image);

        // imgArr.each(function(key, item){
        // for (var i = 0; i < imgArr.length; i++) {
        //     let newImg = $(`<div class="col-12 col-sm-6 col-lg-4 image_box added">
        //                         <label class="tabul_container_item">
        //                             <input class="tabul_container_item_input" type="file" name="photo-${imageCounter++}">
        //                             <img src="${imgArr[i]}" alt="" class="tabul_container_item_image">
        //                         </label>
        //                         <img src="../../../public/images/close.png" alt="" class="tabul_container_item_close">
        //                         <input class="hidden_del_img" type="hidden" name="delete_image" value="${Math.floor(Math.random() * 1000)}">
        //                     </div>`);
        //     $('.add_box').before(newImg);
        //     $('.static_image').remove();
        //     newImg.find('.tabul_container_item_close').click(deleteImage);
        //     // newImg.find('.tabul_container_item_close').click(returnImage);
        //     inputImage();
        // }
        // });
    });

// });

//WORK

    let krasavchik;

    const sendData = ($kukushka, e) => {
        // let url = $kukushka.attr("action");
        // e.preventDefault();
        // let form = document.getElementById('galleryForm');

        // $.ajax({
        //     url: url,
        //     type: 'POST',
        //     headers: HEADERS,
        //     data: new FormData(form),
        //     contentType: false,
        //     processData: false
        // })
        //     .done((res) => {
        //         if (res.status === 'success') {
        //             // console.log(res.data)
        //             location.reload();
        //         }
        //     })
        //     .fail((res) => {
        //         let error = JSON.parse(res.responseText);
        //         console.log(error);
        //     });



        let url = $kukushka.attr("action");
        e.preventDefault();
        let form = document.getElementById('reportForm');
        // let form = document.getElementById('reportForm');
        console.log($kukushka[0]);
        
        
        $.ajax({
            url: url,
            type: 'POST',
            headers: HEADERS,
            data: new FormData($kukushka[0]),
            contentType: false,
            processData: false
        })
            .done((res) => {
                if (res.status === 'success') {
                    console.log(res.data)
                    location.reload();
                }
            })
            .fail((res) => {
                let error = JSON.parse(res.responseText);
                console.log(error);
            });
    };

    function validGallery($form) {
        const arr = [];
        $form.find('input:not([type=hidden]), textarea').each(function (key, item) {
            let value = $(item).val();
            arr.push(value);
            for (let i of arr) {
                if (i === '') {
                    $(item).addClass('err');
                    krasavchik = false;
                    
                } else {
                    $(item).removeClass('err');
                    krasavchik = true;
                }
                
            }
                console.log(krasavchik);
        });
        
    }

    function validReview($form) {
        const arr = [];
        $form.find('input:not([type=hidden]), textarea').each(function (key, item) {
            let value = $(item).val();
            arr.push(value);
            for (let i of arr) {
                if (i === '') {
                    $(item).addClass('err');
                    krasavchik = false;
                    
                } else {
                    $(item).removeClass('err');
                    krasavchik = true;
                }
                
            }
                console.log(krasavchik);
        });

        let forCat = $('.testimonial_container_mod .edit_modal_container_label_image_item').attr('src');
        if(forCat === '' ){
            $('.testimonial_container_mod input[name="del_img"]').val('1');
        }
        
    }

    $('.galleryAdmin_modal_container_form').on('submit', function (e) {
        validGallery($(this));

        if (krasavchik === true) {
            console.log('sendData');
            sendData($(this), e);
        } else {
            console.log('ne delayem');
            e.preventDefault();
        }
        // sendData($(this), e);
    });
    $('.clientAdmin_modal_container').on('submit', function (e) {

        // validGallery($(this));

        // if (krasavchik === true) {
        //     console.log('sendData');
        //     sendData($(this), e);
        // } else {
        //     console.log('ne delayem');
        //     e.preventDefault();
        // }
        sendData($(this), e);


    });
    $('.faq_container_mod').on('submit', function (e) {
        validGallery($(this));

        if (krasavchik === true) {
            console.log('sendData');
            sendData($(this), e);
        } else {
            console.log('ne delayem');
            e.preventDefault();
        }
        // sendData($(this), e);
    });
    $('.testimonial_container_mod').on('submit', function (e) {
        validReview($(this));

        if (krasavchik === true) {
            console.log('sendData');
            sendData($(this), e);
        } else {
            console.log('ne delayem');
            e.preventDefault();
        }
        // sendData($(this), e);
    });
    

//WORK

    $('.galleryAdmin_modal_container_closer').click(function (e) {
        setTimeout(function () {
            $('.image_box.added').remove();
        });
    });

    $(".gallery_position").on("change", function () {
        $.post({
            url: $(this).attr("data-action"),
            headers: HEADERS,
            data: {position: $(this).val()}
        }).then(function (e) {
            window.location.reload();
        });
    });

    $('.delete').on('click', function (e) {
        var isTrue = confirm("Вы уверены, что хотите удалить?");
        if (!isTrue) {
            e.preventDefault();
        }
    });


})();
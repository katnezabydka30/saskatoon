const HEADERS = {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
};


const pageId = $('main').attr('id');

switch (pageId) {
    case 'mainPage': {
        mainSlider();
        menuFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }
    case 'servicesPage': {
        menuFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }
    case 'galleryPage': {
        galleryModalFunc();
        menuFunc();
        gallerySlick();
        galleryMoreFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }
    case 'faqPage': {
        menuFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }
    case 'servicePage': {
        menuFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }

    case 'nowHiringPage': {
        menuFunc();
        desktopMenuFunc();
        reportModal();
        break;
    }

    case 'testimonialsPage': {
        menuFunc();
        reviewsMoreFunc();
        desktopMenuFunc();
        reportModal();
        testimonComment();
        break;
    }
    case 'bookOnlinePage': {
        menuFunc();
        desktopMenuFunc();
        checked();
        reportModal();
    }
}

// Main Page Slick Init
function mainSlider() {
    $('.main_slider_container.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        // autoplay: true,
    });

    $('.services_slider.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<div class="arrow_slider left"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="36" viewBox="0 0 19 36"><defs><path id="b325a" d="M959.21 2239c-.16 0-.32-.03-.47-.09a1.13 1.13 0 0 1-.4-.26 1.16 1.16 0 0 1-.34-.85c0-.32.12-.6.35-.84l15.81-16.08-15.57-15.8a1.18 1.18 0 0 1-.35-.87c0-.34.12-.62.35-.86.23-.23.51-.35.85-.35.33 0 .61.12.84.35l16.37 16.68c.23.24.35.52.35.85 0 .33-.12.6-.35.84l-16.6 16.93c-.12.12-.26.2-.4.26-.16.06-.3.09-.44.09z"/></defs><g><g transform="translate(-958 -2203)"><use fill="#fff" xlink:href="#b325a"/></g></g></svg></div>',
        nextArrow: '<div class="arrow_slider right"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="36" viewBox="0 0 19 36"><defs><path id="b325a" d="M959.21 2239c-.16 0-.32-.03-.47-.09a1.13 1.13 0 0 1-.4-.26 1.16 1.16 0 0 1-.34-.85c0-.32.12-.6.35-.84l15.81-16.08-15.57-15.8a1.18 1.18 0 0 1-.35-.87c0-.34.12-.62.35-.86.23-.23.51-.35.85-.35.33 0 .61.12.84.35l16.37 16.68c.23.24.35.52.35.85 0 .33-.12.6-.35.84l-16.6 16.93c-.12.12-.26.2-.4.26-.16.06-.3.09-.44.09z"/></defs><g><g transform="translate(-958 -2203)"><use fill="#fff" xlink:href="#b325a"/></g></g></svg></div>',
        // autoplay: true,
        // autoplaySpeed: 5000
    });
}

// Menu 
function menuFunc() {
    // $(document).ready(function(){
    //     $('.menu_container').css('display', 'block');
    // });
    $('.header_container_menu').click(function () {
        $(this).toggleClass('active');
        $('.header').toggleClass('active');
        $('.menu_container').toggleClass('active');

    });

    $('[data-tab]').click(function () {
        var target = $(this).attr('data-tab');


        // $(this).siblings('.menu_container_link').removeClass('active');
        $(this).toggleClass('active');
        $(this).parent().toggleClass('active');
        // $('.menu_container_link_box' + target).siblings('.menu_container_link_box').removeClass('active');
        $('.menu_container_link_box' + target).toggleClass('active');
    });
}

// Gallery Slick Init
function gallerySlick() {
    $('.gallery_modal_slider .slider_big .slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider_small .slider',
        dots: true,
        prevArrow: '<div class="arrow_slider"><svg xmlns="http://www.w3.org/2000/svg" class="arrow_slider_item left" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="512px" height="512px"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5   c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z   " fill="#ff3181"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>',
        nextArrow: '<div class="arrow_slider"><svg xmlns="http://www.w3.org/2000/svg" class="arrow_slider_item right" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="512px" height="512px"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5   c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z   " fill="#ff3181"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>',
        // responsive: [{
        //     breakpoint: 601,
        //     settings: {
        //         slidesToShow: 1,
        //         centerMode: false,
        //     }
        // }],
    });
    $('.gallery_modal_slider .slider_small .slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider_big .slider',
        focusOnSelect: true,
        // prevArrow: '<div class="arrow_slider"><svg xmlns="http://www.w3.org/2000/svg" class="arrow_slider_item left" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="512px" height="512px"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5   c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z   " fill="#ff3181"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>',
        // nextArrow: '<div class="arrow_slider"><svg xmlns="http://www.w3.org/2000/svg" class="arrow_slider_item right" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve" width="512px" height="512px"><g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5   c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z   " fill="#ff3181"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>',
        responsive: [{
            breakpoint: 769,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 551,
            settings: {
                slidesToShow: 2
            }
        }
        ],
    });
}

// Gallery Modal Opener
function galleryModalFunc() {
    // data-slick-index

    $('[data-slide]').click(function () {

        var imageData = $(this).attr('data-slide');
        console.log(imageData);

        $('html').css('overflow', 'hidden');
        $('.gallery_modal').removeClass('active');
        $('.gallery_modal' + imageData).addClass('active');
    });



    $('.close_gallery_btn').click(function () {
        $('html').css('overflow', 'auto');
        $('.gallery_modal').removeClass('active');
    });
}

// Gallery SEE MORE
function galleryMoreFunc() {
    $('.galleryBlock_more').click(function () {
        $('.more_section').toggleClass('hidden');
        $('.galleryBlock_more').toggleClass('active');
        if ($('.galleryBlock_more').hasClass('active')) {
            $('.galleryBlock_more_text').text('Close');
        } else {
            $('.galleryBlock_more_text').text('See more');
        }
    });
}

// Reviews SEE MORE
function reviewsMoreFunc() {
    $('.galleryBlock_more').click(function () {
        $('.more_section').toggleClass('hidden');
        $('.galleryBlock_more').toggleClass('active');
        if ($('.galleryBlock_more').hasClass('active')) {
            $('.galleryBlock_more_text').text('Close');
        } else {
            $('.galleryBlock_more_text').text('See more');
        }
    });
}

// Desktop menu

function desktopMenuFunc() {
    $('[data-menu]').on('mouseover', function () {
        let dataMenu = $(this).attr('data-menu');

        $(this).siblings('.header_desc_container_box_row_link').removeClass('active');
        $(this).toggleClass('active');
        $('.header_container_link_box' + dataMenu).siblings('.header_container_link_box').removeClass('active');
        $('.header_container_link_box' + dataMenu).toggleClass('active');
    });
    $('.header_container_link_box').on('mouseover', function () {
        $(this).addClass('active');
    });
    $('.header_container_link_box').on('mouseleave', function () {
        $(this).removeClass('active');
        $('[data-menu]').removeClass('active');
    });
    $('[data-menu]').on('mouseleave', function () {
        $('.header_container_link_box').removeClass('active');
    });
}

// Report Problem Modal
function reportModal() {

    $('.report_mistake').click(function () {
        $('.modal_report').addClass('active');
    });

    $('.close_report_btn').click(function () {
        $('.modal_report').removeClass('active');
    });

    var windowWidth = $(window).width();

    if (windowWidth <= 650) {
        $('.modal_report .overlay').click(function () {
            $('.modal_report').removeClass('active');
        });
    }
}

function testimonComment() {

    $('.add_comment').click(function () {
        $('.modal_comment').addClass('active');
    });

    $('.close_comment_btn').click(function () {
        $('.modal_comment').removeClass('active');
    });

    var windowWidth = $(window).width();

    if (windowWidth <= 650) {
        $('.modal_comment .overlay').click(function () {
            $('.modal_comment').removeClass('active');
        });
    }
}

function checked() {
    $('input[type=checkbox]').click(function () {
        var target = $(this).parents('label').attr('data-accord');
        if ($(this).prop('checked') === true) {
            $(this).parent().siblings('.box_text').addClass('active');
            $(this).parent().siblings('.box_desc').addClass('active');
            $(this).parent().siblings('.box_arrow').addClass('active');
            $('.box_accordeon' + target).addClass('active');
        } else {
            $(this).parent().siblings('.box_text').removeClass('active');
            $(this).parent().siblings('.box_desc').removeClass('active');
            $(this).parent().siblings('.box_arrow').removeClass('active');
            $('.box_accordeon' + target).removeClass('active');
        }
    });
}


//Validation
// function isValid(type, value) {
//     const EMAIL_REGEXP = new RegExp("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
//     // const PHONE_REGEXP = new RegExp("/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/");
//     switch (type) {
//         case 'isNumber': {
//             return !Number.isNaN(+value);
//         }
//         case 'isString': {
//             return Number.isNaN(+value);
//         }
//         case 'isMail': {
//             return EMAIL_REGEXP.test(value);
//         }
//         case 'isContain': {
//             return value !== '';
//         }
//         case 'isChecked': {
//             return value !== '';
//         }
//         default:
//             return true;
//     }
// }

// function validator(data) {
//     let validated = true;
//     console.log(validated);

//     data.forEach(({ input, types }) => {
//         let isError = false;
//         const $input = $(input);
//         const value = $input.val();

//         types.forEach((type) => {
//             const isValidValue = isValid(type, value);
//             if (!isValidValue) {
//                 validated = false;
//                 isError = true;
//             }
//         });
//         if (($input).is('input') || ($input).is('textarea')) {
//             if (isError) {
//                 $input.addClass('err');
//             } else {
//                 $input.removeClass('err');
//             }
//         }
//     });
//     return validated;
// }

// $('.form_container_box_input').on('change', function (e) {
//     const validated = validator([
//         {
//             input: '#user_name',
//             types: ['isContain'],
//         },
//         {
//             input: '#user_lastName',
//             types: ['isContain'],
//         },
//         {
//             input: '#user_mail',
//             types: ['isContain', 'isMail'],
//         },
//         {
//             input: '#user_phone',
//             types: ['isContain', 'isNumber'],
//         },
//         {
//             input: '#user_phone_modal',
//             types: ['isContain', 'isNumber'],
//         },
//         {
//             input: '#user_name_modal',
//             types: ['isChecked'],
//         },
//         {
//             input: '#user_message_modal',
//             types: ['isContain'],
//         }
//     ]);
// });

// $('.form_container_box_btn').on('click', function (e) {

//     const validated = validator([
//         {
//             input: '#user_name',
//             types: ['isContain'],
//         },
//         {
//             input: '#user_lastName',
//             types: ['isContain'],
//         },
//         {
//             input: '#user_mail',
//             types: ['isContain', 'isMail'],
//         },
//         {
//             input: '#user_age',
//             types: ['isContain', 'isNumber'],
//         },
//         {
//             input: '#user_phone',
//             types: ['isContain', 'isNumber'],
//         },
//         {
//             input: '#user_phone_modal',
//             types: ['isContain', 'isNumber'],
//         },
//         {
//             input: '#user_name_modal',
//             types: ['isChecked'],
//         },
//         {
//             input: '#user_message_modal',
//             types: ['isContain'],
//         }
//     ]);
// });




let krasavchik;

const sendDataReport = ($kukushka, e) => {
    let url = $kukushka.attr("action");
    e.preventDefault();
    let form = document.getElementById('reportForm');
    // let form = document.getElementById('reportForm');
    console.log($kukushka);


    $.ajax({
        url: url,
        type: 'POST',
        headers: HEADERS,
        data: new FormData($kukushka[0]),
        contentType: false,
        processData: false
    })
        .done((res) => {
            if (res.status === 'success') {
                console.log(res.data)
                location.reload();
            }
        })
        .fail((res) => {
            let error = JSON.parse(res.responseText);
            console.log(error);
        });
};

function validGallery($form) {
    const arr = [];
    $form.find('input:not([type=hidden]), textarea').each(function (key, item) {
        let value = $(item).val();
        arr.push(value);
        for (let i of arr) {
            if (i === '') {
                $(item).addClass('err');
                krasavchik = false;
            } else {
                $(item).removeClass('err');
                krasavchik = true;
            }
        }
    });
}

function validBooking($form) {
    const arr = [];
    $form.find('input:not([type=hidden]), textarea').each(function (key, item) {
        let value = $(item).val();
        arr.push(value);
        for (let i of arr) {
            if (i === '') {
                $(item).addClass('err');
                krasavchik = false;
            } else {
                $(item).removeClass('err');
                krasavchik = true;
            }
        }
    });
    
    // $form.find('input[type=checkbox]').filter(function (key, item) {
    //     if (item.checked === true) {
    //         $('.box_radio').removeClass('err');
    //     } else {
    //         $('.box_radio').addClass('err');
    //         console.log(item);
    //     }
    // });
    
}

$('.modal_report_container').on('submit', function (e) {
    validGallery($(this));

    if (krasavchik === true) {
        console.log('sendData');
        sendDataReport($(this), e);
    } else {
        console.log('ne delayem');
        e.preventDefault();
    }
});

$('.modal_comment_container').on('submit', function (e) {
    validGallery($(this));

    if (krasavchik === true) {
        console.log('sendData');
        sendDataReport($(this), e);
    } else {
        console.log('ne delayem');
        e.preventDefault();
    }
});

$('.form_container').on('submit', function (e) {
    validBooking($(this));

    if (krasavchik === true) {
        console.log('sendData');
        sendDataReport($(this), e);
    } else {
        console.log('ne delayem');
        e.preventDefault();
    }
});

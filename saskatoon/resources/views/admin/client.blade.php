@extends('admin.layouts.app_admin')

@section('content')

    <main id="clientsAdmin">
        <section class="settingsAdmin_section testimonialsAdmin_section clientAdmin_section">
            @include('admin.navigation')
            <div class="settingAdmin_block">
                <div class="settingAdmin_block_title">
                    <img src="../images/admin/galleryAdmin_big.png" alt="" class="settingAdmin_block_title_icon">
                    <p class="settingAdmin_block_title_text">Clients</p>
                </div>
                <div class="gallery_tabs">
                    <div class="gallery_tabs_addItem">
                        <button data-action="{{route('admin.client.store')}}"
                                class="gallery_tabs_addItem_btn edit_btn addClient">Add New Client
                        </button>
                    </div>
                </div>
                <div class="testimonials_block">

                    <table id="table1" class="testimonials_table active">
                        <tr class="testimonials_table_head">
                            <th class="main_picture">
                                <p>#</p>
                            </th>
                            <th class="main_picture">
                                <p>Client</p>
                            </th>
                            <th class="btn">

                            </th>
                        </tr>
                        @forelse($clients as $key => $client)
                        <tr class="testimonials_table_body">

                                <td>
                                    <p>{{ $key + 1 }}</p>
                                </td>
                                <td class="main_picture">
                                    @isset($client->logo)
                                        <img src=" {{ Storage::disk('upload')->url($client->logo) }}"
                                             class="main_picture_img" alt="">
                                    @endisset
                                </td>


                                <td class="testimonials_table_box">

                                    <div class="testimonials_table_box_btn edit_btn edit_client">
                                        <button data-action="{{route('admin.client.update', $client->id)}}">Edit
                                        </button>
                                    </div>

                                    <form action="{{route('admin.client.block', $client->id)}}" method="GET"
                                          class="testimonials_table_box_btn">
                                        <button type="submit">@if ($client->status) Hide @else Show @endif</button>
                                    </form>

                                    <form action="{{route('admin.client.destroy', $client->id)}}" method="POST"
                                          class="testimonials_table_box_btn delete">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                        <button type="submit">Delete</button>
                                    </form>

                                </td>
                        </tr>
                        @empty

                            <td>
                                <h3 style="text-alight:center">Данные отсутствуют</h3>
                            </td>

                        @endforelse

                    </table>
                </div>
                <div class="clientAdmin_modal">
                    <div class="clientAdmin_modal_overlay"></div>
                    <form class="input_box image_box clientAdmin_modal_container"
                          action="{{route('admin.client.store')}}" method="post">
                        @csrf
                        <input class="method" type="hidden" name="_method" value="">
                        <input type="hidden" name="client_id" value="">

                        <p class="edit_modal_container_item_title">Image</p>
                        <div class="tabul_container row">
                            <div class="col-12 image_box">
                                <label class="tabul_container_item main_image">
                                    <input class="tabul_container_item_input" type="file" name="logo">
                                    <img src="" alt="" class="tabul_container_item_image">
                                </label>
                            </div>
                        </div>
                        <button class="clientAdmin_modal_container_btn">Save</button>
                        <img src="http://127.0.0.1:8080/images/admin/close.png" alt="" class="clients_closer">
                    </form>
                </div>
            </div>
        </section>
    </main>

@endsection

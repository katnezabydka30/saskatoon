@extends('admin.layouts.app_admin')

@section('content')
    <main id="faqsAdmin">
        <section class="settingsAdmin_section testimonialsAdmin_section">
            @include('admin.navigation')
            <div class="settingAdmin_block">
                <div class="settingAdmin_block_title">
                    <img src="{{ asset('images/testimonials_admin.png') }}" alt=""
                         class="settingAdmin_block_title_icon">
                    <p class="settingAdmin_block_title_text">FAQ</p>
                </div>
                <div class="testimonials_block">


                    @if($faqs->lastPage() > 1)
                        <div class="pagination_table">
                            <div class="pagination_table_counter">
                                <span class="pagination_table_counter_current">{{ $faqs->currentPage() }}</span>
                                <span>из</span>
                                <span class="pagination_table_counter_total">{{ $faqs->lastPage() }}</span>
                            </div>
                            <div class="pagination_table_arrows">
                                @if($faqs->currentPage() !== 1)
                                    <a href="{{ $faqs->url(($faqs->currentPage() - 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item prev_arrow"></a>
                                @endif
                                @if($faqs->currentPage() !== $faqs->lastPage())
                                    <a href="{{ $faqs->url(($faqs->currentPage() + 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item next_arrow"></a>
                                @endif
                            </div>
                        </div>
                    @endif


                    <table class="testimonials_table">
                        <tr class="testimonials_table_head">
                            <th>
                                <p>№</p>
                            </th>
                            <th class="name_table">
                                <p>
                                    Question</p>
                            </th>
                            <th class="name_comment">
                                <p>
                                    Answer</p>
                            </th>
                            <th class="name_date">
                                <p>
                                    Date</p>
                            </th>
                            <th class="btn">
                                <div class="create_faq testimonials_table_btn create_testimonial">
                                    <button data-action="{{route('admin.faq.store')}}"> Add New</button>
                                </div>
                            </th>
                        </tr>
                        @forelse($faqs as $key => $faq)
                            <tr class="testimonials_table_body">
                                <td>
                                    <p>{{ $key + 1 }}</p>
                                </td>
                                <td class="name_place faq_quastion">
                                    <p>{{ $faq->question ?? '' }}</p>
                                </td>
                                <td class="name_comment faq_answer">
                                    <p>{{ $faq->answer ?? '' }}</p>
                                </td>
                                <td class="name_date"  data-value="@isset($faq->created_at) {{ $faq->created_at->format('Y-m-d') }} @endisset">
                                    <p>@isset($faq->created_at) {{ $faq->created_at->format('d.m.Y') }} @endisset</p>
                                </td>
                                <td class="testimonials_table_box">
                                    <div class="edit-faq testimonials_table_box_btn edit_btn">
                                        <button data-action="{{route('admin.faq.update', $faq->id)}}">Edit</button>
                                    </div>

                                    <form action="{{route('admin.faq.block', $faq->id)}}" method="GET"
                                          class="testimonials_table_box_btn">
                                        <button type="submit">@if ($faq->status) Hide @else Show @endif</button>
                                    </form>

                                    <form action="{{route('admin.faq.destroy', $faq->id)}}" method="POST"
                                          class="testimonials_table_box_btn delete">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                        <button type="submit">Delete</button>
                                    </form>

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>
                                    <h3 style="text-alight:center">Данные отсутствуют</h3>
                                </td>
                            </tr>
                        @endforelse
                        @if ($errors->any())
                            <tr>
                                @foreach($errors as $error)
                                    <td>
                                        <h3 style="text-alight:center">{{ $error }}</h3>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    </table>


                </div>

                <div class="edit_modal">
                    <div class="overlay"></div>
                    <form class="edit_modal_container faq_container_mod row" action="{{route('admin.faq.store')}}" method="post">
                        <input class="method" type="hidden" name="_method" value="">
                        @csrf
                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-4">
                            <div class="input_box">
                                <p class="edit_modal_container_item_title">Question</p>
                                <textarea name="question" id="" cols="30" rows="10"
                                          class="edit_modal_container_item_textarea modal_question
                                                        @if($errors->first('question')) error @endif">
                                            </textarea>
                            </div>
                        </div>
                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-5">
                            <p class="edit_modal_container_item_title">Answer</p>
                            <textarea name="answer" id="" cols="30" rows="10"
                                      class="edit_modal_container_item_textarea modal_answer
                                                    @if($errors->first('question')) error @endif">
                                             {{$faq->answer ?? old('answer') ?? ''}}
                                        </textarea>
                        </div>
                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-3">
                            <div class="input_box">
                                <p class="edit_modal_container_item_title">Date</p>
                                <input name="created_at" id="date" type="date"
                                       class="edit_modal_container_item_input input_date"
                                       value="">
                            </div>
                            <div class="btn_box">
                                <button type="submit" class="edit_modal_container_item_submit">Save changes</button>
                            </div>
                        </div>
                        <img src="{{ asset('images/close.png') }}" alt="" class="close_edit_modal">
                    </form>
                </div>

            </div>
        </section>

    </main>


@endsection

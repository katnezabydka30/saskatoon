@extends('admin.layouts.app_admin')

@section('content')

    <main id="galleryAdmin">

        <section class="settingsAdmin_section galleryAdmin_section">

            @include('admin.navigation')

            <div class="settingAdmin_block">


                <div class="settingAdmin_block_title">
                    <img src="{{ asset('images/admin/galleryAdmin_big.png') }}" alt=""
                         class="settingAdmin_block_title_icon">
                    <p class="settingAdmin_block_title_text">Gallery</p>
                </div>
                <div class="gallery_tabs">
                    <a href="{{ route('admin.gallery.show', 'residential') }}" class="gallery_tabs_item
                        @if (request()->path() == 'admin/gallery/residential') active @endif ">Residential
                        gallery</a>
                    <a href="{{ route('admin.gallery.show', 'commercial') }}" class="gallery_tabs_item
                            @if (request()->path() == 'admin/gallery/commercial') active @endif ">Commercial
                        gallery</a>
                    <div class="create_gallery gallery_tabs_addItem">
                        <button class="gallery_tabs_addItem_btn"
                                data-action="{{route('admin.gallery.store')}}"> Add New
                        </button>
                    </div>
                    <form action="{{ route('admin.gallery.search', $type) }}" method="GET" class="gallery_tabs_search">
                        <button type="submit" class="gallery_tabs_search_btn"><img
                                    src="{{ asset('images/admin/search_admin.png') }}"
                                    alt="" class="gallery_tabs_search_btn_icon"></button>
                        <input type="text" name="search" class="gallery_tabs_search_input" placeholder="Search">
                    </form>
                </div>


                <div class="testimonials_block">


                    @if($albums->lastPage() > 1)
                        <div class="pagination_table">
                            <div class="pagination_table_counter">
                                <span class="pagination_table_counter_current">{{ $albums->currentPage() }}</span>
                                <span>из</span>
                                <span class="pagination_table_counter_total">{{ $albums->lastPage() }}</span>
                            </div>
                            <div class="pagination_table_arrows">
                                @if($albums->currentPage() !== 1)
                                    <a href="{{ $albums->url(($albums->currentPage() - 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item prev_arrow"></a>
                                @endif
                                @if($albums->currentPage() !== $albums->lastPage())
                                    <a href="{{ $albums->url(($albums->currentPage() + 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item next_arrow"></a>
                                @endif
                            </div>
                        </div>
                    @endif
                    <table class="testimonials_table ">
                        <tr class="testimonials_table_head">
                            <th class="album_name">
                                <p>Album Name</p>
                            </th>
                            <th class="">
                                <p>Position</p>
                            </th>
                            <th class="name_table">
                                <p>Place</p>
                            </th>
                            <th class="name_date">
                                <p>Date</p>
                            </th>
                            <th class="main_picture">
                                <p>Main picture</p>
                            </th>
                            <th class="all_pictures">
                                <p>All pictures</p>
                            </th>
                        </tr>

                        @forelse($albums as $key => $album)
                            <tr class="testimonials_table_body">
                                <td class="album_name">
                                    <p>{{ $album->name ?? '' }}</p>
                                </td>

                                <td>
                                    <div class="form-group styled-select">
                                        <select class="gallery_position styled form-control"  id="inputState"
                                                data-action="{{ route('admin.gallery.position', $album->id)  }}"
                                                name="position">
                                            @foreach($albums_all as $key => $item)
                                                <option value="{{$item->position}}"
                                                        @if($item->position == $album->position) selected @endif>{{ $key + 1 }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td class="name_place">
                                    <p>{{ $album->place ?? '' }}</p>
                                </td>
                                <td class="name_date" data-value="@isset($album->created_at){{ $album->created_at->format('Y-m-d') }}@endisset">
                                    <p>@isset($album->created_at) {{ $album->created_at->format('d.m.Y') }} @endisset</p>
                                </td>
                                <td class="main_picture">
                                    @isset($album->main_photo)
                                        <img src=" {{ Storage::disk('upload')->url($album->main_photo) }}"
                                             class="main_picture_img" alt="">
                                    @endisset
                                </td>
                                <td class="all_pictures">
                                    <div>
                                        @if ($album->photos->count() > 0)
                                            @for ($i = 0; $i < 4; $i++)
                                                @isset($album->photos[$i])
                                                    <img src="{{ Storage::disk('upload')->url($album->photos[$i]->image) }}"
                                                         alt=""
                                                         class="all_pictures_img">
                                                @endisset
                                            @endfor
                                        @endif
                                    </div>
                                    <div class="hidden-galery">
                                        @if ($album->photos->count() > 0)
                                            @foreach ($album->photos as $photo)
                                                    <p src="{{ Storage::disk('upload')->url($photo->image) }}" data-id="{{$photo->id}}"></p>
                                            @endforeach
                                        @endif
                                    </div>
                                </td>

                                <td class="testimonials_table_box">
                                    <div class="edit-gallery testimonials_table_box_btn edit_btn">
                                        <button data-id="{{$album->id}}" data-action="{{route('admin.gallery.update', $album->id)}}">Edit
                                        </button>
                                    </div>
                                    <form action="{{route('admin.gallery.block', $album->id)}}" method="GET"
                                          class="testimonials_table_box_btn">
                                        <button type="submit">@if ($album->status) Hide @else Show @endif</button>
                                    </form>
                                    <form action="{{route('admin.gallery.destroy', $album->id)}}" method="POST"
                                          class="testimonials_table_box_btn delete">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                        <button type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>
                                    <h3 style="text-alight:center">Данные отсутствуют</h3>
                                </td>
                            </tr>
                        @endforelse
                        @if ($errors->any())
                            <tr>
                                @foreach($errors as $error)
                                    <td>
                                        <h3 style="text-alight:center">{{ $error }}</h3>
                                    </td>
                                @endforeach
                            </tr>
                        @endif

                    </table>
                </div>


                <div class="galleryAdmin_modal">
                    <div class="overlay"></div>
                    <div class="galleryAdmin_modal_container">


                        <form enctype="multipart/form-data" id="galleryForm" class="galleryAdmin_modal_container_form"
                              action="{{route('admin.gallery.store')}}" method="post">
                            <input class="method" type="hidden" name="_method" value="">
                            <input class="count" type="hidden" name="count" value="">
                            <input type="hidden" name="type" value="{{ $type ?? '' }}">
                            <input type="hidden" name="album_id" value="">
                            @csrf

                            <div class="galleryAdmin_modal_container_box">
                                <p class="galleryAdmin_modal_container_box_title">Album Name</p>
                                <input type="text" name="name"
                                       class="galleryAdmin_modal_container_box_input album_name">
                            </div>
                            <div class="galleryAdmin_modal_container_box">
                                <p class="galleryAdmin_modal_container_box_title">Place</p>
                                <input type="text" name="place"
                                       class="galleryAdmin_modal_container_box_input place_input">
                            </div>

                            <div class="galleryAdmin_modal_container_box">
                                <p class="galleryAdmin_modal_container_box_title">Date</p>
                                <input name="created_at" id="date" type="date"
                                       class="galleryAdmin_modal_container_box_input date_input"
                                       value="">
                            </div>

                            <div class="relativeBlock">
                                <input type="hidden" name="deleted_images" value="">
                                <div class="tabul active" id="tabul1">
                                    <p class="tabul_text">Main picture</p>
                                    <div class="tabul_container row">
                                        <div class="col-12 col-sm-6 col-lg-4 image_box">
                                            <label class="tabul_container_item main_image">
                                                <input class="tabul_container_item_input" type="file" name="main_photo">
                                                    <img src="#" alt="" class="tabul_container_item_image">
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4 image_box static_image">
                                            <label class="tabul_container_item">
                                                <input class="tabul_container_item_input" type="file" name="photo">
                                                <img src="#" alt="" class="tabul_container_item_image">
                                            </label>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4 add_box">
                                            <p class="add_box_item">+</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="galleryAdmin_modal_container_btn">Save</button>
                        </form>
                        <img src="{{ asset('images/admin/close.png') }}" alt=""
                             class="galleryAdmin_modal_container_closer">
                    </div>
                </div>

            </div>
        </section>
    </main>
@endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin panel</title>

    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/slick/slick.css') }}">

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">


    <link
        href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900&amp;subset=cyrillic,cyrillic-ext"
        rel="stylesheet">
</head>
<body>

@yield('content')


<!-- MyScripts -->
<script src="{{ asset('libs/jquery-3.3.1/jquery.min.js') }}" defer></script>
<script src="{{ asset('libs/bootstrap-4.2.1/js/bootstrap.min.js') }}" defer></script>

<script src="{{ asset('js/admin.js') }}" defer></script>

</body>
</html>

<div class="settingAdmin_nav">
    <a href="{{ route('index') }}" class="settingAdmin_nav_link">
        <img src="{{ asset('images/admin/logo.svg') }}" alt="" class="settingAdmin_nav_link_image">
    </a>
    <div class="settingAdmin_list">
        <a href="{{ route('admin.testimonial.index') }}" class="settingAdmin_list_item @if (Route::currentRouteName() == 'admin.testimonial.index') current-page @endif">
            <img src="{{ asset('images/admin/testimonialsAdmin.png')}}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">Testimonials</p>
        </a>
        <a href="{{ route('admin.faq.index') }}" class="settingAdmin_list_item @if (Route::currentRouteName() == 'admin.faq.index') current-page @endif">
            <img src="{{ asset('images/admin/faqAdmin.png')}}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">FAQ</p>
        </a>
        <a href="{{ route('admin.gallery.show', 'residential') }}" class="settingAdmin_list_item
                @if (request()->path() == 'admin/gallery/residential') current-page @endif">
            <img src="{{ asset('images/admin/galleryAdmin.png')}}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">Gallery</p>
        </a>
        <a href="{{ route('admin.client.index') }}" class="settingAdmin_list_item
               @if (Route::currentRouteName() == 'admin.client.index') current-page @endif">
            <img src="{{ asset('images/admin/avatar.png')}}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">Clients</p>
        </a>
        <a href="{{ route('admin.setting') }}" class="settingAdmin_list_item @if (Route::currentRouteName() == 'admin.setting') current-page @endif">
            <img src="{{ asset('images/admin/settingsAdmin.png')}}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">Settings</p>
        </a>
    </div>

    <form class="exit_item_admin" action="{{ route('logout') }}" method="POST">
        @csrf
        <button>
            <img src="{{ asset('images/admin/exit.svg') }}" alt="" class="settingAdmin_list_item_icon">
            <p class="settingAdmin_list_item_text">Exit</p>
        </button>
    </form>
</div>
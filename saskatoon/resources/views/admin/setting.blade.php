@extends('admin.layouts.app_admin')

@section('content')

    <main id="settingsAdmin">

        <section class="settingsAdmin_section">

            @include('admin.navigation')

            <div class="settingAdmin_block">
                <div class="settingAdmin_block_title">
                    <img src="{{ asset('images/admin/settingAdmin_title.png') }}" alt="" class="settingAdmin_block_title_icon">
                    <p class="settingAdmin_block_title_text">Settings</p>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach (\Session::get('error') as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            @foreach (\Session::get('success') as $success)
                                <li>{{$success}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <p class="settingAdmin_block_desc">Change Password</p>
                <form class="settingAdmin_form" action="{{route('admin.setting.update', $user)}}" method="post">
                    <div class="settingAdmin_form_row">
                        <div class="settingAdmin_form_item">
                            <p class="settingAdmin_form_item_desc">Old Password*</p>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="password" name="current-password" class="settingAdmin_form_item_input">
                        </div>
                        <div class="settingAdmin_form_item">
                            <p class="settingAdmin_form_item_desc">New Password*</p>
                            <input type="password" name="password" class="settingAdmin_form_item_input">
                        </div>
                        <div class="settingAdmin_form_item">
                            <p class="settingAdmin_form_item_desc">Confirm Password*</p>
                            <input type="password" name="password_confirmation" class="settingAdmin_form_item_input">
                        </div>
                    </div>
                    <div class="settingAdmin_form_item">
                        <input type="submit" value="Save New Password" class="settingAdmin_form_item_input input_submit_btn">
                    </div>
                </form>

            </div>
        </section>
    </main>


@endsection

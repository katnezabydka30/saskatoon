@extends('admin.layouts.app_admin')

@section('content')


    <main id="testimonialsAdmin">
        <section class="settingsAdmin_section testimonialsAdmin_section">
            @include('admin.navigation')
            <div class="settingAdmin_block">
                <div class="settingAdmin_block_title">
                    <img src="{{ asset('images/admin/testimonials_admin.png') }}" alt="" class="settingAdmin_block_title_icon">
                    <p class="settingAdmin_block_title_text">Testimonials</p>
                </div>
                <div class="testimonials_block">

                    @if($testimonials->lastPage() > 1)
                        <div class="pagination_table">
                            <div class="pagination_table_counter">
                                <span class="pagination_table_counter_current">{{ $testimonials->currentPage() }}</span>
                                <span>из</span>
                                <span class="pagination_table_counter_total">{{ $testimonials->lastPage() }}</span>
                            </div>
                            <div class="pagination_table_arrows">
                                @if($testimonials->currentPage() !== 1)
                                    <a href="{{ $testimonials->url(($testimonials->currentPage() - 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item prev_arrow"></a>
                                @endif
                                @if($testimonials->currentPage() !== $testimonials->lastPage())
                                    <a href="{{ $testimonials->url(($testimonials->currentPage() + 1 )) }}"><img
                                                src="{{ asset('images/admin_arrow.svg') }}" alt=""
                                                class="pagination_table_arrows_item next_arrow"></a>
                                @endif
                            </div>
                        </div>
                    @endif



                    <table class="testimonials_table">
                        <tr class="testimonials_table_head">
                            <th>
                                <p>№</p>
                            </th>
                            <th class="table_favic">
                                <p>Image</p>
                            </th>
                            <th class="name_table">
                                <p>
                                    Name</p>
                            </th>
                            <th class="name_place">
                                <p>
                                    Place</p>
                            </th>
                            <th class="name_comment">
                                <p>
                                    Comment</p>
                            </th>
                            <th class="name_date">
                                <p>
                                    Date</p>
                            </th>
                            <th class="btn">
                                <div class="create_testimonial testimonials_table_btn">
                                    <button data-action="{{route('admin.testimonial.store')}}"> Add New</button>
                                </div>
                            </th>
                        </tr>
                        @forelse($testimonials as $key => $testimonial)
                        <tr class="testimonials_table_body">

                            <td>
                                <p>{{ $key + 1 }}</p>
                            </td>
                            <td class="table_favic">
                                @isset($testimonial->image)
                                    <img src=" {{ Storage::disk('upload')->url($testimonial->image) }}" class="table_favic_image" alt="">
                                @endisset
                            </td>
                            <td class="name_table name_test">
                                <p>{{ $testimonial->name ?? '' }}</p>
                            </td>
                            <td class="name_place place_test">
                                <p>{{ $testimonial->place ?? '' }}</p>
                            </td>
                            <td class="name_comment comment_test">
                                <p>{{ $testimonial->comment ?? '' }}</p>
                            </td>
                            <td class="name_date" data-value="@isset($testimonial->created_at) {{ $testimonial->created_at->format('Y-m-d') }} @endisset">
                                <p>@isset($testimonial->created_at) {{ $testimonial->created_at->format('d.m.Y') }} @endisset</p>
                            </td>
                            <td class="testimonials_table_box">
                                <div class="edit-testimonial testimonials_table_box_btn edit_btn edit-testimonial">
                                    <button data-action="{{route('admin.testimonial.update', $testimonial->id)}}">Edit</button>
                                </div>

                                <form action="{{route('admin.testimonial.block', $testimonial->id)}}" method="GET"
                                      class="testimonials_table_box_btn">
                                    <button type="submit">@if ($testimonial->status) Hide @else Show @endif</button>
                                </form>

                                <form action="{{route('admin.testimonial.destroy', $testimonial->id)}}" method="POST"
                                      class="testimonials_table_box_btn delete">
                                    <input type="hidden" name="_method" value="delete">
                                    {{ csrf_field() }}
                                    <button type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td>
                                    <h3 style="text-alight:center">Данные отсутствуют</h3>
                                </td>
                            </tr>
                        @endforelse
                        @if ($errors->any())
                            <tr>
                                @foreach($errors as $error)
                                    <td>
                                        <h3 style="text-alight:center">{{ $error }}</h3>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    </table>
                </div>



                <div class="edit_modal">
                    <div class="overlay"></div>
                    <form class="edit_modal_container testimonial_container_mod row" action="{{route('admin.testimonial.store')}}" method="post">
                        <input class="method" type="hidden" name="_method" value="">
                        <input class="del_img" type="hidden" name="del_img" value="">
                        @csrf
                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-4">
                            <div class="input_box">
                                <p class="edit_modal_container_item_title">Name</p>
                                <textarea name="name" id="" cols="30" rows="10" class="edit_modal_container_item_input name_area"></textarea>
                            </div>
                            <div class="input_box">
                                <p class="edit_modal_container_item_title">Place</p>
                                <textarea name="place" id="" cols="30" rows="10" class="edit_modal_container_item_input place_area"></textarea>
                            </div>
                            <div class="input_box image_box">
                                <p class="edit_modal_container_item_title">Image</p>
                                <div class="input_box_image_file">
                                    <label for="imageFile1" class="edit_modal_container_label_image">
                                        <input type="file" id="imageFile1" name="image"/>
                                        <p class="edit_modal_image_label">Choose file</p>
                                    </label>
                                    <div>
                                        <img src="" alt="" class="edit_modal_container_label_image_item">
                                        <img src="{{ asset('images/admin/close.png') }}" alt="" class="input_file_close">
                                        <input class="hidden_del_review_img" type="hidden" name="delete_image" value="123">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-5 fl-start">
                            <p class="edit_modal_container_item_title">Comment</p>
                            <textarea name="comment" id="" cols="30" rows="10" class="edit_modal_container_item_textarea comment_area"></textarea>
                        </div>
                        <div class="edit_modal_container_item col-12 col-md-6 col-lg-3">
                            <div class="input_box">
                                <p class="edit_modal_container_item_title">Date</p>
                                <input name="created_at" id="date" type="date"
                                       class="edit_modal_container_item_input input_date"
                                       value="">
                            </div>
                            <div class="btn_box">
                                <button type="submit"
                                       class="edit_modal_container_item_submit">Save changes</button>
                            </div>
                        </div>
                        <img src="{{ asset('images/admin/close.png') }}" alt="" class="close_edit_modal">
                    </form>
                </div>
            </div>
        </section>
    </main>
@endsection
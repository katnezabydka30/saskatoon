@extends('admin.layouts.app_admin')

@section('content')

    <main id="mainAdmin">
        <section class="admin_section">
            <form method="POST" action="{{ route('login') }}" class="log_form">
                @csrf
                <a class="log_form_image" href="{{ route('index') }}">
                    <img src="{{ asset('images/admin/logo.svg') }}" alt="">
                </a>
                <p class="log_form_desc">Email*</p>
                <input type="text" class="log_form_input" placeholder="Email" name="email" value="{{ old('email') }}">
                <p class="log_form_desc">Password*</p>
                <input type="password" class="log_form_input" name="password">

                <input type="submit" value="Login" class="log_form_btn">
                @if ($errors->any())
                    <p class="error_text">Incorrect login or password</p>
                @endif
            </form>
        </section>
    </main>

@endsection

@extends('layouts.app')

@section('content')

    <main id="bookOnlinePage">
        <section class="bookOnline_section">
            <div class="wrapper">
                <p class="bookOnline_title">Complete Your Booking</p>
                <p class="bookOnline_desc">Please provide a few details to complete your booking.</p>
            </div>
        </section>
        <section class="hiring_form book-online_form">
            <div class="wrapper">
                <form action="" class="form_container row">
                    <div class="form_container_box col-12 col-md-6">
                        <p class="form_container_box_title">Choose your service</p>
                        <div class="form_container_box_list">
                            <label class="form_container_box_list_item box_radio">
                                <div class="box_radio">
                                    <input type="checkbox" class="box_radio_input">
                                </div>
                                <span class="box_text">House cleaning</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Office cleaning</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Moving day cleaning</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Preparation for sale</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label data-accord="#accord1" class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Washing carpet and upholstery with shampoo</span>
                                <span class="box_arrow">
                                    <img src="{{ asset('images/arrow_book.svg') }}" alt="" class="box_arrow_image">
                                </span>
                            </label>
                            <div class="box_accordeon" id="accord1">
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Hallway = $90</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Rug= $80</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms and Hallway = $140</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing andHallway = $170</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing, Hallway and Rug = $220</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing and Hallway = $249</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing, Hallway and Rug = $260</p>
                                </label>
                            </div>
                            <label data-accord="#accord2" class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Washing windows</span>
                                <span class="box_arrow">
                                    <img src="{{ asset('images/arrow_book.svg') }}" alt="" class="box_arrow_image">
                                </span>
                            </label>
                            <div class="box_accordeon" id="accord2">
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Hallway = $90</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Rug= $80</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms and Hallway = $140</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing andHallway = $170</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing, Hallway and Rug = $220</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing and Hallway = $249</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing, Hallway and Rug = $260</p>
                                </label>
                            </div>
                            <label class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Grand opening</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Deep cleaning</span>
                                <span class="box_desc">(min charge 180$)</span>
                            </label>
                            <label data-accord="#accord3" class="form_container_box_list_item">
                                <div class="box_radio">
                                    <input type="checkbox" name="" id="" class="box_radio_input">
                                </div>
                                <span class="box_text">Post construction clean-up</span>
                                <span class="box_arrow">
                                    <img src="{{ asset('images/arrow_book.svg') }}" alt="" class="box_arrow_image">
                                </span>
                            </label>
                            <div class="box_accordeon" id="accord3">
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Hallway = $90</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">1 Room and Rug= $80</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms and Hallway = $140</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing andHallway = $170</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">3 Rooms, One Set of Stairs,
                                        Landing, Hallway and Rug = $220</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing and Hallway = $249</p>
                                </label>
                                <label class="box_accordeon_item">
                                    <input type="checkbox" name="" id="" class="box_accordeon_item_checkbox">
                                    <p class="box_accordeon_item_text">4 Rooms, Two Set of Stairs,
                                        Landing, Hallway and Rug = $260</p>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form_container_box col-12 col-md-6">
                        <p class="form_container_box_title">Write information</p>
                        <p class="form_container_box_desc">Furthermore, our Microfiber Cloths trap more dust and grime
                            than paper based or cotton cleaning cloths, meaning you get the ultimate clean every
                            time.</p>
                        <div class="form_container_box_small_item">
                            <p class="small_item_text">Floor area:</p>
                            <input type="text" class="small_item_input">
                        </div>
                        <div class="form_container_box_small_item">
                            <p class="small_item_text">Cleaners:</p>
                            <input type="text" class="small_item_input">
                        </div>
                        <div class="form_container_box_small_item">
                            <p class="small_item_text">Hours:</p>
                            <input type="text" class="small_item_input">
                        </div>
                    </div>
                    <div class="form_container_box col-12 col-md-6">
                        <p class="form_container_box_title">Who you are?</p>
                        <p class="form_container_box_desc">This information will be used to contact you</p>
                        <input type="text" id="user_book_name" class="form_container_box_input" placeholder="Name*">
                        <input type="text" id="user_book_lastName" class="form_container_box_input"
                               placeholder="Last Name*">
                        <input type="email" id="user_book_mail" class="form_container_box_input" placeholder="E-mail*">
                        <input type="tel" id="user_book_phone" class="form_container_box_input" placeholder="Phone*">
                        <input type="text" id="user_book_adress" class="form_container_box_input"
                               placeholder="Your Address*">
                    </div>
                    <div class="form_container_box col-12 col-md-6">
                        <p class="form_container_box_title">Place and time of cleaning</p>
                        <p class="form_container_box_desc bold_text">Where is cleaning needed?</p>
                        <input type="text" id="user_book_city" class="form_container_box_input" placeholder="City*">
                        <input type="text" id="user_book_cleanAdress" class="form_container_box_input"
                               placeholder="Address*">
                        <p class="form_container_box_desc bold_text">Where is cleaning needed?</p>
                        <input type="date"
                               id="user_book_date" class="form_container_box_input" placeholder="Date*">
                        <input type="tel" id="user_book_time" class="form_container_box_input" placeholder="Time*">
                        <input type="text" id="user_book_message" class="form_container_box_input"
                               placeholder="Message">
                    </div>
                    <div class="form_container_box col-12 form_container_box_w100">
                        <p class="form_container_box_title">Payment Details</p>
                        <p class="form_container_box_dexcription">In the event of a cancellation, $50 will be
                            deducted from my account.</p>
                        <div class="row">
                            <div class="form_container_box_card col-12 col-md-6">
                                <input type="text" id="user_book_cardName" class="form_container_box_input"
                                       placeholder="Your Name on card*">
                                <input type="text" id="user_book_cardNumber" class="form_container_box_input"
                                       placeholder="Card Number*">
                                <input type="text" id="user_book_cardDate" class="form_container_box_input"
                                       placeholder="Expiry Date*">
                            </div>
                            <div class="form_container_box_card col-12 col-md-6">
                                <input type="text" id="user_book_cardCVV" class="form_container_box_input"
                                       placeholder="CVV*">
                                <input type="tel" id="user_book_cardZIP" class="form_container_box_input"
                                       placeholder="ZIP/POSTAL*">
                                <button type="submit" id="user_book_submit"
                                       class="form_container_box_input form_container_box_input-submit">Book Now</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>

@endsection







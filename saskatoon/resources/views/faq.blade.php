@extends('layouts.app')

@section('content')

    <main id="faqPage">
        <section class="faq_section">
            <div class="wrapper">
                <p class="faq_title">FAQ</p>
                <p class="faq_desc">Frequently Asked Questions</p>
                @foreach($faqs as $faq)
                    <div class="faq_container">
                        <p class="faq_container_title">{{ $faq->question ?? '' }}</p>
                        <p class="faq_container_text">{{ $faq->answer ?? '' }}</p>
                    </div>
                @endforeach

            </div>
        </section>
    </main>

@endsection

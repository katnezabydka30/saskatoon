@extends('layouts.app')

@section('content')

    <main id="galleryPage">
        <section class="galleryBlock">
            <p class="galleryBlock_title">Gallery</p>
            <p class="galleryBlock_desc">Residential</p>
            <div class="wrapper">
                <div class="gallery_container row">

                    @foreach($albums as $key => $album)

                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal{{$key}}">
                        <div> @if ($album->main_photo)
                                <img class="gallery_container_box_image"
                                     src="{{ Storage::disk('upload')->url($album->main_photo) }}">
                            @endif
                        </div>
                        <p class="gallery_container_box_name">{{ $album->name ?? '' }} </p>
                    </div>
                    <div class="gallery_modal" id="gal{{ $key }}">
                        <div class="gallery_modal_slider">
                            <img src="{{ asset('images/close.png') }} " alt="" class="close_gallery_btn">
                            <div class="slider">
                                <div class="slide">
                                    <div class="gallery_modal_slider_box">
                                        <div class="gallery_modal_slider_box_header">
                                            <p class="gallery_modal_slider_box_header_title">{{ $album->place ?? '' }}</p>
                                            <p class="gallery_modal_slider_box_header_date"><b>Date:</b>{{ $album->created_at->format('d M Y') ?? '' }}</p>
                                        </div>
                                        <div class="slider_big">
                                            <div class="slider">
                                                @foreach($album->photos as $photo)
                                                <div class="slide">
                                                    <img src="{{ Storage::disk('upload')->url($photo->image) }}" alt="" class="slider_big_image">
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="slider_small">
                                            <div class="slider">
                                                @foreach($album->photos as $photo)
                                                    <div class="slide">
                                                        <img src="{{ Storage::disk('upload')->url($photo->image) }}" alt="" class="slider_big_image">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

                </div>
            </div>
            <div class="wrapper more_section hidden">
                <div class="gallery_container row">
                </div>
            </div>
            <div class="galleryBlock_more">
                <p class="galleryBlock_more_text">See more</p>
                <img src="{{ asset('images/arrow_pink.png') }}" alt="" class="galleryBlock_more_arrow">
            </div>
        </section>
    </main>


@endsection

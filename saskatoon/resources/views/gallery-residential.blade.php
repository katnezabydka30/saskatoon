@extends('layouts.app')

@section('content')

    <main id="galleryPage">
        <section class="galleryBlock">
            <p class="galleryBlock_title">Gallery</p>
            <p class="galleryBlock_desc">Residential</p>
            <div class="wrapper">
                <div class="gallery_container row">
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal0">
                        <div>
                            <img src="{{ asset('images/gallery1.png') }}" alt="" class="gallery_container_box_image">
                        </div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal1">
                        <div>
                            <img src="{{ asset('images/gallery2.png') }}" alt="" class="gallery_container_box_image">
                        </div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal2">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal3">
                        <div>
                            <img src="{{ asset('images/gallery1.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal4">
                        <div>
                            <img src="{{ asset('images/gallery2.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal5">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal6">
                        <div>
                            <img src="{{ asset('images/gallery1.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>

                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal7">
                        <div>
                            <img src="{{ asset('images/gallery2.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal8">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                </div>
            </div>
            <div class="wrapper more_section hidden">
                <div class="gallery_container row">
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal9">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal10">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                    <div class="gallery_container_box col-12 col-sm-6 col-lg-4" data-slide="#gal11">
                        <div>
                            <img src="{{ asset('images/gallery3.png') }}" alt="" class="gallery_container_box_image"></div>
                        <p class="gallery_container_box_name">Private house in Saskatoon, <br><b>Сleaning before
                                moving.</b> </p>
                    </div>
                </div>
            </div>
            <div class="galleryBlock_more">
                <p class="galleryBlock_more_text">See more</p>
                <img src="{{ asset('images/arrow_pink.png') }}" alt="" class="galleryBlock_more_arrow">
            </div>
        </section>
    </main>

    <div>
        <div class="gallery_modal" id="gal0">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal1">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal2">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal3">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal4">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal5">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal6">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal7">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal8">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal9">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal10">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery_modal" id="gal11">
            <div class="gallery_modal_slider">
                <img src="images/close.png" alt="" class="close_gallery_btn">
                <div class="slider">
                    <div class="slide">
                        <div class="gallery_modal_slider_box">
                            <div class="gallery_modal_slider_box_header">
                                <p class="gallery_modal_slider_box_header_title">Private house in Saskatoon, <br>
                                    <b>Сleaning before moving.</b></p>
                                <p class="gallery_modal_slider_box_header_date"><b>Date:</b> 12 Sep 2018</p>
                            </div>
                            <div class="slider_big">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_big_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_big_image">
                                    </div>
                                </div>
                            </div>
                            <div class="slider_small">
                                <div class="slider">
                                    <div class="slide">
                                        <img src="images/gallery3.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery2.png" alt="" class="slider_small_image">
                                    </div>
                                    <div class="slide">
                                        <img src="images/gallery1.png" alt="" class="slider_small_image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

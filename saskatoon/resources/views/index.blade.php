@extends('layouts.app')

@section('content')

    <main id="mainPage">
        <section class="first_section">
            <div class="wrapper">
                <div class="first_section_container">
                    <div class="first_section_container_box">
                        <div class="circle"></div>
                        <p class="first_section_container_box_title">House
                            <br>
                            Cleaning Services</p>
                        <p class="first_section_container_box_desc">He felicity no an at packages answered opinions
                            juvenile. Is inquiry no he several excited am. If as increasing contrasted entreaties be.
                            If as increasing contrasted entreaties be. Secure shy favour length all twenty denote. Any
                            delicate you how kindness horrible outlived servants. If as increasing contrasted
                            entreaties be. To sure calm </p>
                        <br>
                        <p class="first_section_container_box_desc"> An concluded sportsman offending so provision mr
                            education. Course sir people worthy horses add entire suffer. Sportsman do offending
                            supported extremity breakfast by listening. If as increasing contrasted entreaties be. Draw
                            from upon here gone add one. Polite do object at passed it is. Happiness remainder joy but
                            earnestly for off.</p>
                    </div>
                </div>
                <img src="{{ asset('images/title_woman1.png') }}" alt="" class="first_section_image">
            </div>
        </section>
        <section class="main_slider">
            <div class="main_slider_container slider">
                <div class="slide">
                    <div class="main_slider_container_box" style="background:url({{ asset('images/mainSlider.png') }}) center/cover no-repeat">
                        <div class="wrapper">

                            <p class="main_slider_container_box_title">We are an Eco Friendly Cleaning Company</p>
                            <a href="#" class="main_slider_container_box_link">See more</a>
                        </div>
                    </div>
                </div>
                <div class="slide">
                    <div class="main_slider_container_box" style="background:url({{ asset('images/mainSlider.png') }}) center/cover no-repeat">
                        <div class="wrapper">
                            <p class="main_slider_container_box_title">We are an Eco Friendly Cleaning Company</p>
                            <a href="#" class="main_slider_container_box_link">See more</a>
                        </div>

                    </div>
                </div>
                <div class="slide">
                    <div class="main_slider_container_box" style="background:url({{ asset('images/mainSlider.png') }}) center/cover no-repeat">
                        <div class="wrapper">
                            <p class="main_slider_container_box_title">We are an Eco Friendly Cleaning Company</p>
                            <a href="#" class="main_slider_container_box_link">See more</a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section class="services">
            <div class="wrapper">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Always clean with us</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">Polite do object at passed it is. Decisively advantages nor expression
                    unpleasing she led met.</p>
                <div class="services_container row">
                    <div class="services_container_box col-12 col-sm-6 col-md-4">
                        <p class="services_container_title">OUR SERVICES</p>
                        <a href="{{ route('services') }}">
                            <img src="{{ asset('images/service1.svg') }}" alt="" class="services_container_image">
                        </a>
                        <p class="services_container_desc">Polite do object at passed it is. Decisively advantages nor
                            expression unpleasing she led met.</p>
                    </div>
                    <div class="services_container_box col-12 col-sm-6 col-md-4">
                        <p class="services_container_title">NOW HIRING</p>
                        <a href="{{ route('now-hiring') }}">
                        <img src="{{ asset('images/service2.svg') }}" alt="" class="services_container_image">
                        </a>
                        <p class="services_container_desc">Polite do object at passed it is. Decisively advantages nor
                            expression unpleasing she led met.</p>
                    </div>
                    <div class="services_container_box col-12 col-sm-6 col-md-4">
                        <p class="services_container_title">BOOK ONLINE</p>
                        <a href="{{ route('booking') }}">
                        <img src="{{ asset('images/service3.svg') }}" alt="" class="services_container_image">
                        </a>
                        <p class="services_container_desc">Polite do object at passed it is. Decisively advantages nor
                            expression unpleasing she led met.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="reviews">
            <div class="overlay"></div>
            <div class="wrapper">
                <img src="{{ asset('images/buble.png') }}" alt="" class="buble_box">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Testimonials</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">Polite do object at passed it is. Decisively advantages nor expression
                    unpleasing she led met.</p>
                <div class="services_slider slider">
                    @foreach($testimonials as $testimonial)
                    <div class="slide">
                        <div class="services_slider_box">
                            <div class="services_slider_box_item">
                                @if ($testimonial->image)
                                    <img class="services_slider_box_item_image" src="{{ Storage::disk('upload')->url($testimonial->image) }}">
                                @else
                                    <img src="{{ asset('images/review1.png') }}" alt="" >
                                @endif
                            </div>
                            <div class="services_slider_box_text">
                                <div class="box_title">{{ $testimonial->name ?? '' }}</div>
                                <p class="box_text">{{ $testimonial->comment ?? '' }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="customers">
            <div class="wrapper">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Our Customers</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">Polite do object at passed it is. Decisively advantages nor expression
                    unpleasing she led met.</p>
                <div class="customers_container row">
                    @foreach($clients as $client)
                    <div class="customers_container_item col-12 col-sm-6 col-lg-3">
                        <img src="{{ Storage::disk('upload')->url($client->logo) }}" alt="" class="customers_container_item_image">
                        {{--<img src="{{ asset('images/citrus.png') }}" alt="" class="customers_container_item_image">--}}
                    </div>
                    @endforeach
                    {{--<div class="customers_container_item col-12 col-sm-6 col-lg-3">--}}
                        {{--<img src="{{ asset('images/sprint.png') }}" alt="" class="customers_container_item_image">--}}
                    {{--</div>--}}
                    {{--<div class="customers_container_item col-12 col-sm-6 col-lg-3">--}}
                        {{--<img src="{{ asset('images/burgerKing.png') }}" alt="" class="customers_container_item_image">--}}
                    {{--</div>--}}
                    {{--<div class="customers_container_item col-12 col-sm-6 col-lg-3">--}}
                        {{--<img src="{{ asset('images/company.png') }}" alt="" class="customers_container_item_image">--}}
                    {{--</div>--}}
                </div>
            </div>
        </section>
    </main>

@endsection







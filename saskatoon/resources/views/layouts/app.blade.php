<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('meta')">
    <meta name="description" content="@yield('desc')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-4.2.1/css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/slick/slick-theme.css') }}">

    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

</head>
<body>

@section('header')
    @include('layouts.header')
@show

@yield('content')


@section('footer')
    @include('layouts.footer')
@show


{{--<!-- Scripts -->--}}
<script src="{{ asset('libs/jquery-3.3.1/jquery.min.js') }}" defer></script>
<script src="{{ asset('libs/bootstrap-4.2.1/js/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('libs/slick/slick.min.js') }}" defer></script>

<script src="{{ asset('js/main.js') }}" defer></script>
</body>

</html>

<footer class="footer">
    <div class="wrapper">
        <div class="footer_container row">
            <div class="footer_container_item col-7 col-sm-3">
                <a href="/" class="footer_container_item_logo">
                    <img src="{{ asset('images/logo.svg') }}" alt="" class="logo_image">
                </a>
                <div class="footer_container_item_social">
                    <p class="footer_container_item_social_title">Social Media</p>
                    <a href="#" class="footer_container_item_social_link">
                        <img src="{{ asset('images/insta.svg') }}" alt="" class="social_icon">
                    </a>
                    <a href="#" class="footer_container_item_social_link">
                        <img src="{{ asset('images/fb.svg') }}" alt="" class="social_icon">
                    </a>
                </div>
            </div>
            <div class="footer_container_item col-12 col-sm-6">
                <p class="footer_container_item_text">Сall center</p>
                <a href="tel: 306 491 0168" class="footer_container_item_tel">
                    <svg xmlns="http://www.w3.org/2000/svg" class="tel_icon" xmlns:xlink="http://www.w3.org/1999/xlink"
                         width="27" height="27" viewBox="0 0 27 27">
                        <defs>
                            <path id="ihxna" d="M828.4 115.42a11 11 0 0 0-10.98 10.98 11 11 0 0 0 10.98 10.98 11 11 0 0 0 10.98-10.98 11 11 0 0 0-10.98-10.98m0-2.42a13.4 13.4 0 1 1 0 26.8 13.4 13.4 0 0 1 0-26.8m7.14 17.68c.17.5 0 1.23-.38 1.6l-1.02 1.02c-.19.19-.72.3-.74.3a12.1 12.1 0 0 1-12.2-12.23s.12-.52.3-.7l1.03-1.02c.37-.38 1.1-.55 1.6-.38l.21.07c.5.17 1.03.72 1.17 1.24l.52 1.89c.14.51-.05 1.24-.42 1.62l-.69.68a7.26 7.26 0 0 0 5.11 5.12l.69-.69c.37-.37 1.1-.56 1.62-.42l1.89.52c.51.14 1.07.66 1.24 1.16l.07.22" />
                        </defs>
                        <g>
                            <g clip-path="url(#clip-1C887FAA-CBAF-23AA-C784-799AB2B38143)" transform="translate(-815 -113)">
                                <use fill="#ff3181" xlink:href="#ihxna" />
                            </g>
                        </g>
                    </svg>
                    <p class="tel_text">306.491.0168</p>
                </a>
                <a href="mailto:clean.saskatoon@gmail.com" class="footer_container_item_text">clean.saskatoon@gmail.com</a>
                <a href="{{ route('booking') }}" class="footer_container_item_link">Book a Cleaning</a>
                <a class="footer_container_item_link report_mistake">Report a problem <br/> or mistake on this page</a>
            </div>
            <div class="footer_container_item nav_footer col-5 col-sm-3">
                <a href="{{ route('index') }}" class="footer_container_item_nav">Main page</a>
                <a href="{{ route('services') }}" class="footer_container_item_nav">Our services</a>
                <a href="{{ route('booking') }}" class="footer_container_item_nav">Book online</a>
                <a href="{{ route('faq') }}" class="footer_container_item_nav">FAQ</a>
                <a href="{{ route('now-hiring') }}" class="footer_container_item_nav">Now hiring</a>
                <a href="{{ route('gallery', 'commercial') }}" class="footer_container_item_nav">Commercial gallery</a>
                <a href="{{ route('gallery', 'residential') }}" class="footer_container_item_nav">Residential gallery</a>
                <a href="{{ route('testimonial') }}" class="footer_container_item_nav">Testimonials</a>
            </div>
        </div>

    </div>
    <p class="footer_copyright">Copyright © 2019 Cleaning Saskatoon - Command+X</p>
</footer>


<div class="menu_container d-block d-xl-none">
    <div class="menu_box">

        <div class="menu_container_list">
            <a href="/" class="menu_container_link">Main page</a>
            <a class="menu_container_link" data-tab="#mob1">Our services <img src="{{ asset('images/arrow_pink.png') }}" alt=""
                                                                              class="menu_container_link_icon"></a>
            <div class="menu_container_link_box row" id="mob1">
                <div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('service-house-cleaning') }}" class="item_link">House cleaning</a>
                        <a href="{{ route('service-office-cleaning') }}" class="item_link">Office cleaning</a>
                    </div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('service-moving-day-cleaning') }}" class="item_link">Moving day cleaning</a>
                        <a href="{{ route('service-preparation-for-sale') }}" class="item_link">Preparation for sale</a>
                    </div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('service-washing-carpet') }}" class="item_link">Washing carpet <br>
                            and upholstery <br>
                            with shampoo</a>
                    </div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('service-washing-windows') }}" class="item_link">Washing windows</a>
                        <a href="{{ route('service-grand') }}" class="item_link">Grand opening</a>
                    </div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('service-deep-cleaning') }}" class="item_link">Deep cleaning</a>
                        <a href="{{ route('service-post-construction-clean-up') }}" class="item_link">Post construction clean-up</a>
                    </div>
                </div>
            </div>
            <a href="{{ route('index') }}" class="menu_container_link">Book online</a>
            <a href="{{ route('faq') }}" class="menu_container_link">FAQ</a>
            <a href="{{ route('now-hiring') }}" class="menu_container_link">Now hiring</a>
            <a class="menu_container_link" data-tab="#mob2">Gallery <img src="{{ asset('images/arrow_pink.png') }}" alt="" class="menu_container_link_icon"></a>
            <div class="menu_container_link_box row" id="mob2">
                <div>
                    <div class="menu_container_link_box_item">
                        <a href="{{ route('gallery', 'commercial') }}" class="item_link">Commercial gallery</a>

                    </div>

                    <div class="menu_container_link_box_item">
                        <a href="{{ route('gallery', 'residential') }}" class="item_link">Residential gallery</a>
                    </div>
                </div>
            </div>
            <a href="{{ route('testimonial') }}" class="menu_container_link">Testimonials</a>
        </div>
        <div class="menu_container_footer">
            <a href="#" class="menu_container_footer_link">
                <img src="{{ asset('images/insta.svg') }}" alt="" class="link_icon">
            </a>
            <p class="menu_container_footer_text">Copyright © 2019 Cleaning Saskatoon - Command+X</p>
            <a href="#" class="menu_container_footer_link">
                <img src="{{ asset('images/fb.svg') }}" alt="" class="link_icon">
            </a>
        </div>
    </div>
</div>

<div class="modal_report">
    <div class="overlay"></div>
    <form class="modal_report_container" id="reportForm">
        <img src="{{ asset('images/close.png') }} " alt="" class="close_report_btn">
        <p class="modal_report_container_title">Report a problem or mistake on this page</p>
        <p class="modal_report_container_text">Thank you for helping us become better!</p>
        <input type="text" class="modal_report_container_input" placeholder="Name*">
        <input type="text" class="modal_report_container_input" placeholder="Phone number*">
        <textarea name="" id="" cols="30" rows="10" class="modal_report_container_textarea" placeholder="Lune for messages*"></textarea>
        <button type="submit" class="modal_report_container_btn">Send information</button>
    </form>
</div>
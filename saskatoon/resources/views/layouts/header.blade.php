<header class="header">
    <div class="wrapper">
        <div class="header_desc_container">
            <a href="{{ route('index') }}" class="header_desc_container_logo">
                <img src="{{ asset('images/logo.svg') }}" alt="" class="header_container_logo_item">
            </a>
            <div class="header_desc_container_box">
                <div class="header_desc_container_box_row">
                    <a href="mailto:clean.saskatoon@gmail.com" class="header_info_item">
                        <img src="{{ asset('images/mail.svg') }}" alt="" class="header_info_item_icon">
                        <p class="header_info_item_text">clean.saskatoon@gmail.com</p>
                    </a>
                    <a href="tel: 306 491 0168" class="header_info_item">
                        <img src="{{ asset('images/tel.svg') }}" alt="" class="header_info_item_icon">
                        <p class="header_info_item_text">306.491.0168</p>
                    </a>
                    <a href="{{ route('booking') }}" class="header_container_link">Book a Cleaning</a>
                </div>
                <div class="header_desc_container_box_row">
                    <a href="{{ route('index') }}" class="header_desc_container_box_row_link">Main page</a>
                    <a href="{{ route('services') }}" data-menu="#menu1" class="header_desc_container_box_row_link">Our services</a>
                    <a href="{{ route('booking') }}" class="header_desc_container_box_row_link">Book online</a>
                    <a href="{{ route('faq') }}" class="header_desc_container_box_row_link">FAQ</a>
                    <a href="{{ route('now-hiring') }}" class="header_desc_container_box_row_link">Now hiring</a>
                    <a data-menu="#menu2" class="header_desc_container_box_row_link">Gallery</a>
                    <a href="{{ route('testimonial') }}" class="header_desc_container_box_row_link">Testimonials</a>

                    <div class="header_container_link_box row" id="menu1">
                        <div class="header_container_link_box_item">
                            <a href="{{ route('service-house-cleaning') }}" class="item_link">House cleaning</a>
                            <a href="{{ route('service-office-cleaning') }}" class="item_link">Office cleaning</a>
                        </div>
                        <div class="header_container_link_box_item">
                            <a href="{{ route('service-moving-day-cleaning') }}" class="item_link">Moving day cleaning</a>
                            <a href="{{ route('service-preparation-for-sale') }}" class="item_link">Preparation for sale</a>
                        </div>
                        <div class="header_container_link_box_item">
                            <a href="{{ route('service-washing-carpet') }}" class="item_link">Washing carpet <br>
                                and upholstery <br>
                                with shampoo</a>
                        </div>
                        <div class="header_container_link_box_item">
                            <a href="{{ route('service-washing-windows') }}" class="item_link">Washing windows</a>
                            <a href="{{ route('service-grand') }}" class="item_link">Grand opening</a>
                        </div>
                        <div class="header_container_link_box_item">
                            <a href="{{ route('service-deep-cleaning') }}" class="item_link">Deep cleaning</a>
                            <a href="{{ route('service-post-construction-clean-up') }}" class="item_link">Post construction clean-up</a>
                        </div>
                    </div>
                    <div class="header_container_link_box row" id="menu2">
                        <div class="header_container_link_box_item">
                            <a href="{{ route('gallery', 'commercial') }}" class="item_link">Commercial gallery</a>
                        </div>
                        <div class="header_container_link_box_item">
                            <a href="{{ route('gallery', 'residential') }}" class="item_link">Residential gallery</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_container header_mob">
        <a href="/" class="header_container_logo">
            <img src="{{ asset('images/logo.svg') }}" alt="" class="header_container_logo_item">
            <img src="{{ asset('images/logo_mob.svg') }}" alt="" class="header_container_logo_item_mob">
        </a>
        <a href="{{ route('booking') }}" class="header_container_link">Book a Cleaning</a>
        <div class="header_container_menu">
            <span class="header_container_menu_item"></span>
            <span class="header_container_menu_item"></span>
            <span class="header_container_menu_item"></span>
        </div>
    </div>
    <div class="header_info header_mob">
        <a href="mailto:clean.saskatoon@gmail.com" class="header_info_item">
            <img src="{{ asset('images/logo.svg') }}" alt="" class="header_info_item_icon">
            <p class="header_info_item_text">clean.saskatoon@gmail.com</p>
        </a>
        <a href="tel: 306 491 0168" class="header_info_item">
            <img src="{{ asset('images/tel.svg') }}" alt="" class="header_info_item_icon">
            <p class="header_info_item_text">306.491.0168</p>
        </a>
    </div>
</header>
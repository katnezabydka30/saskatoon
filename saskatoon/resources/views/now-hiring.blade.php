@extends('layouts.app')

@section('content')

    <main id="nowHiringPage">
        <section class="about_hiring">
            <div class="shadow_box"></div>
            <div class="wrapper">
                <div class="hiring_container row">
                    <div class="hiring_container_box col-12 col-lg-7">
                        <img src="{{ asset('images/hiring.png') }}" alt="" class="hiring_container_box_image">
                    </div>
                    <div class="hiring_container_box col-12 col-lg-5">
                        <p class="hiring_container_box_title">Now hiring</p>
                        <p class="hiring_container_box_desc">An concluded sportsman offending so provision mr
                            education. Course sir people worthy horses add entire suffer. Sportsman do offending
                            supported extremity breakfast by listening. If as increasing contrasted entreaties be. Draw
                            from upon here gone add one. </p>
                        <p class="hiring_container_box_list_title">The advantages of working with us</p>
                        <ul class="hiring_container_box_list">
                            <li class="list_item">All surfaces dusted

                            <li class="list_item">Carpet and area rugs vacuumed</li>

                            <li class="list_item">Upholstered furniture vacuumed</li>

                            <li class="list_item">Cushions and pillows fluffed and straightened out</li>

                            <li class="list_item">Glass tables cleaned</li>

                            <li class="list_item">Light organizing</li>
                        </ul>
                    </div>
                </div>
                <ul class="hiring_container_box_list_mob">
                    <p class="list_title">The advantages of working with us</p>
                    <li class="list_item">All surfaces dusted

                    <li class="list_item">Carpet and area rugs vacuumed</li>

                    <li class="list_item">Upholstered furniture vacuumed</li>

                    <li class="list_item">Cushions and pillows fluffed and straightened out</li>

                    <li class="list_item">Glass tables cleaned</li>

                    <li class="list_item">Light organizing</li>
                </ul>
            </div>
        </section>
        <section class="requirements">
            <div class="wrapper">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Requirements</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">The advantages of working with us</p>
                <div class="require_container row">
                    <div class="require_container_box col-12 col-md-6">
                        <ul class="require_container_box_list">
                            <li class="list_item">All surfaces dusted

                            <li class="list_item">Carpet and area rugs vacuumed</li>

                            <li class="list_item">Upholstered furniture vacuumed</li>

                            <li class="list_item">Cushions and pillows fluffed and straightened out</li>

                            <li class="list_item">Glass tables cleaned</li>

                            <li class="list_item">Light organizing</li>
                        </ul>
                    </div>
                    <div class="require_container_box col-12 col-md-6">
                        <ul class="require_container_box_list">
                            <li class="list_item">All surfaces dusted

                            <li class="list_item">Carpet and area rugs vacuumed</li>

                            <li class="list_item">Upholstered furniture vacuumed</li>

                            <li class="list_item">Cushions and pillows fluffed and straightened out</li>

                            <li class="list_item">Glass tables cleaned</li>

                            <li class="list_item">Light organizing</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="hiring_form">
            <div class="wrapper">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Fill in the form</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">Answer the questions and send us a request</p>
                <form action="" class="form_container row">
                    <div class="form_container_box col-12 col-md-5">
                        <p class="form_container_box_title">Who you are?</p>
                        <p class="form_container_box_desc">This information will be used to contact you</p>
                        <input type="text" id="user_name" class="form_container_box_input" placeholder="Name*">
                        <input type="text" id="user_lastName" class="form_container_box_input" placeholder="Last Name*">
                        <input type="email" id="user_mail" class="form_container_box_input" placeholder="E-mail*">
                        <input type="tel" id="user_phone" class="form_container_box_input" placeholder="Phone*">
                        <input type="text" id="user_age" class="form_container_box_input" placeholder="Your Age*">
                    </div>
                    <div class="form_container_box col-12 col-md-5">
                        <p class="form_container_box_title">Information</p>
                        <p class="form_container_box_desc">Are you eligible to work in Canada?*</p>
                        <div class="form_container_box_radio">
                            <div>
                                <input type="radio" name="workCanada" id="workCanadaYes" class="form_container_box_radio_item">
                            </div>
                            <label for="workCanadaYes" class="form_container_box_radio_label">Yes</label>
                            <div>
                                <input type="radio" name="workCanada" id="workCanadaNo" class="form_container_box_radio_item">
                            </div>
                            <label for="workCanadaNo" class="form_container_box_radio_label">No</label>
                        </div>
                        <p class="form_container_box_desc">Years of Relevant Experience*</p>
                        <div class="form_container_box_radio">
                            <div>
                                <input type="radio" name="expCanada" id="expCanadaNone" class="form_container_box_radio_item">
                            </div>

                            <label for="expCanadaNone" class="form_container_box_radio_label">None</label>
                            <div>
                                <input type="radio" name="expCanada" id="expCanadaLess1" class="form_container_box_radio_item">
                            </div>

                            <label for="expCanadaLess1" class="form_container_box_radio_label">Less than 1 year</label>
                        </div>
                        <div class="form_container_box_radio">
                            <div>
                                <input type="radio" name="expCanada" id="expCanadaLess3" class="form_container_box_radio_item">
                            </div>

                            <label for="expCanadaLess3" class="form_container_box_radio_label">1-3 years</label>
                            <div>
                                <input type="radio" name="expCanada" id="expCanadaLess9" class="form_container_box_radio_item">
                            </div>

                            <label for="expCanadaLess9" class="form_container_box_radio_label">4-9 years</label>
                            <div>
                                <input type="radio" name="expCanada" id="expCanadaMore10" class="form_container_box_radio_item">
                            </div>

                            <label for="expCanadaMore10" class="form_container_box_radio_label">10+ years</label>
                        </div>
                        <textarea name="" id="" cols="30" rows="10" class="form_container_box_textarea" placeholder="Your massage"></textarea>
                        <button type="submit" class="form_container_box_btn">Send</button>
                    </div>
                </form>
            </div>
        </section>
    </main>

@endsection

@extends('layouts.app')

@section('content')

    <main id="servicePage">
        <section class="service_first">
            <div class="wrapper">
                <div class="first_container">
                    <div class="first_container_item">
                        <img src="{{ asset('images/window.svg') }}" alt="" class="first_container_item_icon">
                        <div class="first_container_item_box">
                            <p class="box_title">Washing windows</p>
                            <p class="box_desc">Our properly trained cleaning team is ready for your apartment</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_list">
            <div class="wrapper">
                <div class="list_container row">
                    <div class="list_container_box col-12 col-md-6">
                        <ul class="list_container_box_item">
                            <li>
                                All surfaces dusted
                            </li>
                            <li>
                                Carpet and area rugs vacuumed
                            </li>
                            <li>
                                Upholstered furniture vacuumed
                            </li>
                            <li>
                                Cushions and pillows fluffed and straightened out
                            </li>
                            <li>
                                Glass tables cleaned
                            </li>
                            <li>
                                Light organizing
                            </li>
                        </ul>
                    </div>
                    <div class="list_container_box col-12 col-sm-8 col-md-5">
                        <p class="list_container_box_text">Living in an apartment building can contribute to more dust
                            accumulation and poorer air quality than traditional homes due to centralized, shared
                            ventilation systems.
                            Because of this, finding an apartment cleaning service that can adequately remove irritants
                            and harmful particles from the air can help insure that your home is healthy and safe.
                            <br> <br>
                            Furthermore, our Microfiber Cloths trap more dust and grime than paper based or cotton
                            cleaning cloths, meaning you get the ultimate clean every time.</p>
                        <a href="#" class="list_container_box_btn">Book a Washing windows</a>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection

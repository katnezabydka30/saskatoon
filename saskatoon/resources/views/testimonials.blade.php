@extends('layouts.app')

@section('content')

    <main id="testimonialsPage">
        <section class="testimonials_section">
            <div class="wrapper">
                <div class="services_title">
                    <span class="services_title_line"></span>
                    <p class="services_title_text">Testimonials</p>
                    <span class="services_title_line"></span>
                </div>
                <p class="services_desc">Polite do object at passed it is. Decisively advantages nor expression
                    unpleasing she led met.</p>
                <a class="add_comment">Leave your comment</a>
                <div class="testimonials_container">
                    @foreach($testimonials as $key => $testimonial)
                        <div class="testimonials_container_box @if (!($key % 2)) left_oriented @else right_oriented @endif ">
                            <img src="{{ asset('images/bubles_pink.png') }}" alt="" class="termonials_bubles">
                            @if ($testimonial->image)
                                <img class="testimonials_container_box_image"
                                     src="{{ Storage::disk('upload')->url($testimonial->image) }}">
                            @else
                                <img src="{{ asset('images/review1.png') }}" alt=""
                                     class="testimonials_container_box_image">
                            @endif

                            <p class="testimonials_container_box_title">{{ $testimonial->name ?? '' }}</p>
                            <p class="testimonials_container_box_text">{{ $testimonial->comment ?? '' }}</p>
                            <p class="testimonials_container_box_date">
                                {{ $testimonial->created_at->format('F d, Y') ?? '' }}
                            </p>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="wrapper more_section hidden">
                {{--<div class="testimonials_container">--}}
                    {{--<div class="testimonials_container_box left_oriented">--}}
                        {{--<img src="{{ asset('images/bubles_pink.png') }}" alt="" class="termonials_bubles">--}}
                        {{--<img src="{{ asset('images/review1.png') }}" alt="" class="testimonials_container_box_image">--}}
                        {{--<p class="testimonials_container_box_title">Helen, director of Beauty Salon «Muse»</p>--}}
                        {{--<p class="testimonials_container_box_text">He felicity no an at packages answered opinions--}}
                            {{--juvenile. Is inquiry no he several excited am. If as increasing contrasted entreaties be. If--}}
                            {{--as increasing contrasted entreaties be. Secure shy favour length all twenty denote. Any--}}
                            {{--delicate you how kindness horrible outlived servants. If as increasing contrasted entreaties--}}
                            {{--be. To sure calm</p>--}}
                        {{--<p class="testimonials_container_box_date">January 25, 2019</p>--}}
                    {{--</div>--}}
                    {{--<div class="testimonials_container_box right_oriented">--}}
                        {{--<img src="{{ asset('images/bubles_pink.png') }}" alt="" class="termonials_bubles">--}}
                        {{--<img src="{{ asset('images/review2.png') }}" alt="" class="testimonials_container_box_image">--}}
                        {{--<p class="testimonials_container_box_title">Sam, Saskatoon</p>--}}
                        {{--<p class="testimonials_container_box_text">He felicity no an at packages answered opinions--}}
                            {{--juvenile. Is inquiry no he several excited am. If as increasing contrasted entreaties be. If--}}
                            {{--as increasing contrasted entreaties be. Secure shy favour length all twenty denote. Any--}}
                            {{--delicate you how kindness horrible outlived servants. If as increasing contrasted entreaties--}}
                            {{--be. To sure calm</p>--}}
                        {{--<p class="testimonials_container_box_date">January 25, 2019</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="galleryBlock_more">
                <p class="galleryBlock_more_text">See more</p>
                <img src="{{ asset('images/arrow_pink.png') }}" alt="" class="galleryBlock_more_arrow">
            </div>
        </section>
    </main>

    <div class="modal_comment">
        <div class="overlay"></div>
        <form class="modal_comment_container">
            <img src="{{ asset('images/close.png') }} " alt="" class="close_comment_btn">
            <p class="modal_comment_container_title">Leave your comment</p>
            <p class="modal_comment_container_text">Thank you for helping us become better!</p>
            <input type="text" class="modal_comment_container_input" placeholder="Name*">
            <input type="tel" class="modal_comment_container_input" placeholder="Phone number*">
            <textarea name="" id="" cols="30" rows="10" class="modal_comment_container_textarea" placeholder="Lune for messages*"></textarea>
            <input type="submit" value="Send information" class="modal_comment_container_btn">
        </form>
    </div>

@endsection

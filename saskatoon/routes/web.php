<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('/services', 'ServiceController@services')->name('services');
Route::get('/booking', 'IndexController@booking')->name('booking');
Route::get('/faq', 'IndexController@faq')->name('faq');
Route::get('/now-hiring', 'IndexController@nowHiring')->name('now-hiring');
//Route::get('/gallery-commercial', 'IndexController@galleryCommercial')->name('gallery.commercial');
//Route::get('/gallery-residential', 'IndexController@galleryResidential')->name('gallery.residential');
Route::get('/gallery-{commercial}', 'IndexController@Gallery')->name('gallery');
Route::get('/testimonial', 'IndexController@testimonial')->name('testimonial');

Route::get('/service-house-cleaning', 'ServiceController@serviceHouse')->name('service-house-cleaning');
Route::get('/service-office-cleaning', 'ServiceController@serviceOffice')->name('service-office-cleaning');
Route::get('/service-moving-day-cleaning', 'ServiceController@serviceMoving')->name('service-moving-day-cleaning');
Route::get('/service-preparation-for-sale', 'ServiceController@servicePreparation')->name('service-preparation-for-sale');
Route::get('/service-washing-carpet', 'ServiceController@serviceCarpet')->name('service-washing-carpet');
Route::get('/service-washing-windows', 'ServiceController@serviceWindows')->name('service-washing-windows');
Route::get('/service-grand', 'ServiceController@serviceGrand')->name('service-grand');
Route::get('/service-deep-cleaning', 'ServiceController@serviceDeep')->name('service-deep-cleaning');
Route::get('/service-post-construction-clean-up', 'ServiceController@servicePostConstruction')->name('service-post-construction-clean-up');



Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Auth::routes();
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'IndexController@index')->name('admin.index');
    Route::resource('/testimonial', 'TestimonialController', ['as' => 'admin']);
    Route::get('/testimonial/{id}/block', 'TestimonialController@block', ['as' => 'admin'])->name('admin.testimonial.block');
    Route::resource('/faq', 'FaqController', ['as' => 'admin']);
    Route::get('/faq/{id}/block', 'FaqController@block', ['as' => 'admin'])->name('admin.faq.block');

    Route::resource('/gallery', 'AlbumController', ['as' => 'admin']);
    Route::post('/gallery/{id}/position', 'AlbumController@changePosition', ['as' => 'admin'])->name('admin.gallery.position');
    Route::get('gallery/{type}', 'AlbumController@show', ['as' => 'admin'])->name('admin.gallery.search');
    Route::get('/gallery/{id}/block', 'AlbumController@block', ['as' => 'admin'])->name('admin.gallery.block');


    Route::get('/setting', 'IndexController@setting')->name('admin.setting');
    Route::post('/setting', 'IndexController@update')->name('admin.setting.update');

    Route::resource('/client', 'ClientController', ['as' => 'admin']);
    Route::get('/client/{id}/block', 'ClientController@block', ['as' => 'admin'])->name('admin.client.block');


});


const mix = require('laravel-mix');

// Add near top of file
let ImageminPlugin = require( 'imagemin-webpack-plugin' ).default;


mix.webpackConfig( {
    plugins: [
        new ImageminPlugin( {
//            disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '95-100',
            },
            test: /\.(jpe?g|png|gif|svg)$/i,
        } ),
    ],
} )
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/admin.js', 'public/js');
mix.js('resources/js/main.js', 'public/js');

mix.copy( 'resources/images', 'public/images', false );

// mix.minify('public/css/main.css');
// mix.minify('public/css/admin.css');
// mix.minify('public/css/libs.css');
